export dir=/home/ad.msoe.edu/yoder/athcomm/
export log=$dir/log.build.txt
cd $dir
echo "" >> $log
echo "Starting docker build" >> $log 2>&1
sudo docker build -t athcomm -f dockerfile . >> $log 2>&1
echo "Build end time:" >> $log
date >> $log
