const mongoUnit = require('mongo-unit');
const MongoClient = require('mongodb').MongoClient;
const testData = require('./support/student-schedules-test-file.json');
const pipeline = require('../node_express_server/studentSchedulesAggPipeline.js');
const assert = require('chai').assert;

describe('Testing the studentSchedulesAggPipeline to assert that class days are added to the database elements correctly', () => {
  let db;

  beforeEach(() => {
    mongoUnit.load(testData);
    const client = new MongoClient(mongoUnit.getUrl(), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = client.db("test");
  });

  afterEach(() => {
    mongoUnit.drop();
  })

  it('Given that the data is entered with valid M,W,F options class_days array should contain 2,4,6', async function() {
    const testCursor = db.collection("student-schedules").aggregate(pipeline);
    await testCursor.toArray();
    const entry = await db.collection("student-schedules").findOne({_id: "0"}).catch(err => console.log("Error finding db entry:\n", err));

    let class_days = entry.class_days;
    assert.equal(3, class_days.length, "Should be three class days in class_days array.");
    assert.isTrue(class_days.includes(2));
    assert.isTrue(class_days.includes(4));
    assert.isTrue(class_days.includes(6));
  });

  it('Given that the data is entered with an incorrect TUESDAY_CDE set to K then the database connection should not be interrupted and the system should continue as if no TUESDAY_CDE was enterd.', async function() {
    const testCursor = db.collection("student-schedules").aggregate(pipeline);
    await testCursor.toArray();
    const entry = await db.collection("student-schedules").findOne({_id: "1"}).catch(err => console.log("Error finding db entry:\n", err));

    let class_days = entry.class_days;
    assert.equal(1, class_days.length, "Should be one class day in class_days array.");
    assert.isTrue(class_days.includes(5));
  });
});
