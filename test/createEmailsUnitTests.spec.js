const mongoUnit = require('mongo-unit');
const MongoClient = require('mongodb').MongoClient;
const assert = require('chai').assert;
const indexMethods = require('../node_express_server/index.js');
const { ObjectId } = require('mongodb');
const fs = require('fs');
const path = 'C:/Projects/Athcomm/athcomm/newfile.txt';

describe('Testing the createEmails() method in node_express_server/index.js', () => {
  let db;

  beforeEach(() => {
    const client = new MongoClient(mongoUnit.getUrl(), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = client.db("athcomm");
  });

  afterEach(() => {
    mongoUnit.drop();
    if (fs.existsSync(path)) {
      fs.unlinkSync(path);
    }
  })

  // Line 793 -> 919 -> 920 -> 953
  it('The database is empty. No emails should be created.', async function () {
    await indexMethods.createEmails();

    // There should be no emails in the system
    assert.equal(0, await db.collection("emails").count());
  });

  // Line 793 -> 801 -> 803 -> 793 -> 919 -> 920 -> 953
  it('One conflict exists and no email has been created for it yet.', async function () {
    // Insert data
    let athlete = await db.collection("athletes").insertOne({
      "Student Email": "jacobj@msoe.edu",
      Name: "John Jacobs",
      Sport: "msoc"
    });
    let event = await db.collection("events").insertOne({
      Date: "3/31/2023",
      Opponent: "Oshkosh",
      Time: "12:00:00 PM",
      Location: "Oshkosh",
      "Departure Time": "8:00:00 AM",
      Sport: "msoc"
    });
    let classSchedule = await db.collection("student-schedules").insertOne({
      STUDENT_EMAIL: "jacobj@msoe.edu",
      YR_CDE: 2023,
      TRM_CDE: "Q3",
      CRS_CDE: "SE 3030 002",
      room_cde: "DH410",
      MONDAY_CDE: "",
      TUESDAY_CDE: "",
      WEDNESDAY_CDE: "W",
      THURSDAY_CDE: "",
      FRIDAY_CDE: "F",
      SATURDAY_CDE: "",
      BEGIN_TIM: "9:00:00 AM",
      END_TIM: "10:50:00 AM",
      BEGIN_DTE: "1/01/2000",
      END_DTE: "5/24/3000",
      Professor: "Yoder, Josiah",
      PROFESSOR_EMAIL: "yoder@msoe.edu"
    });

    // Create conflicts, then emails
    await indexMethods.createConflicts(db);
    await indexMethods.createEmails(db);

    // There should be one emails in the system
    assert.equal(1, await db.collection("emails_to_send").count());

    // The email should be unsent
    let email = await db.collection("emails_to_send").findOne({
      roster_id: ObjectId(athlete.insertedId),
      class_id: ObjectId(classSchedule.insertedId),
      event_id: ObjectId(event.insertedId)
    });
    assert.equal(false, email.sent);
  });

  // Line 798 -> 806 -> 821 -> 904 -> 907 -> 798 -> 924 -> 925 -> 958
  it('Email has already been sent and no change has been detected.', async function () {
    // Create a "sent" email object that matches 3 event/roster/class objects
    let athlete = await db.collection("athletes").insertOne({
      "Student Email": "jacobj@msoe.edu",
      Name: "John Jacobs",
      Sport: "msoc"
    });
    let event = await db.collection("events").insertOne({
      Date: "3/31/2023",
      Opponent: "Oshkosh",
      Time: "12:00:00 PM",
      Location: "Oshkosh",
      "Departure Time": "8:00:00 AM",
      Sport: "msoc"
    });
    let classSchedule = await db.collection("student-schedules").insertOne({
      STUDENT_EMAIL: "jacobj@msoe.edu",
      YR_CDE: 2023,
      TRM_CDE: "Q3",
      CRS_CDE: "SE 3030 002",
      room_cde: "DH410",
      MONDAY_CDE: "",
      TUESDAY_CDE: "",
      WEDNESDAY_CDE: "W",
      THURSDAY_CDE: "",
      FRIDAY_CDE: "F",
      SATURDAY_CDE: "",
      BEGIN_TIM: "9:00:00 AM",
      END_TIM: "10:50:00 AM",
      BEGIN_DTE: "1/01/2000",
      END_DTE: "5/24/3000",
      Professor: "Yoder, Josiah",
      PROFESSOR_EMAIL: "yoder@msoe.edu"
    });

    // Create conflicts, then emails
    await indexMethods.createConflicts(db);
    await indexMethods.createEmails(db);

    // Update that email to be "sent"
    await db.collection("emails_to_send").findOneAndUpdate(
      {
        roster_id: ObjectId(athlete.insertedId),
        class_id: ObjectId(classSchedule.insertedId),
        event_id: ObjectId(event.insertedId)
      },
      { $set: { sent: true } }
    );

    // Recreate conflicts, then emails
    await indexMethods.createConflicts(db);
    await indexMethods.createEmails(db);

    // assert that the conflict object is remarked as sent
    let email = await db.collection("emails_to_send").findOne({
      roster_id: ObjectId(athlete.insertedId),
      class_id: ObjectId(classSchedule.insertedId),
      event_id: ObjectId(event.insertedId)
    });
    assert.equal(true, email.sent);
  });

  // Line 798 -> 806 -> 821 -> 827 -> 884 -> 886 -> 798 -> 924 -> 925 -> 958
  it('An email has been created and change has occured to the contents of the email. Email has not yet been sent so do not send a follow up email.', async function () {
    // Enter information for a single conflict into the database.
    let athlete = await db.collection("athletes").insertOne({
      "Student Email": "jacobj@msoe.edu",
      Name: "John Jacobs",
      Sport: "msoc"
    });
    let event = await db.collection("events").insertOne({
      Date: "3/31/2023",
      Opponent: "Oshkosh",
      Time: "12:00:00 PM",
      Location: "Oshkosh",
      "Departure Time": "8:00:00 AM",
      Sport: "msoc"
    });
    let classSchedule = await db.collection("student-schedules").insertOne({
      STUDENT_EMAIL: "jacobj@msoe.edu",
      YR_CDE: 2023,
      TRM_CDE: "Q3",
      CRS_CDE: "SE 3030 002",
      room_cde: "DH410",
      MONDAY_CDE: "",
      TUESDAY_CDE: "",
      WEDNESDAY_CDE: "W",
      THURSDAY_CDE: "",
      FRIDAY_CDE: "F",
      SATURDAY_CDE: "",
      BEGIN_TIM: "9:00:00 AM",
      END_TIM: "10:50:00 AM",
      BEGIN_DTE: "1/01/2000",
      END_DTE: "5/24/3000",
      Professor: "Yoder, Josiah",
      PROFESSOR_EMAIL: "yoder@msoe.edu"
    });

    // Generate the conflict and email for this data
    await indexMethods.createConflicts(db);
    await indexMethods.createEmails(db);

    // Make change to the departure time (1 hour earlier)
    await db.collection("events").findOneAndUpdate(
      {
        _id: event.insertedId
      },
      {
        $set: { "Departure Time": "7:00:00 AM" }
      }
    );

    // Recreate conflicts, then emails
    await indexMethods.createConflicts(db);
    await indexMethods.createEmails(db);

    // Assert that the email is marked as unsent
    let email = await db.collection("emails_to_send").findOne({
      roster_id: ObjectId(athlete.insertedId),
      class_id: ObjectId(classSchedule.insertedId),
      event_id: ObjectId(event.insertedId)
    });
    assert.equal(false, email.sent);

    // Assert that the email's description has been updated.
    let conflictDescription = "John Jacobs has a departure time of 7:00:00 AM for a sports competition and may miss SE 3030 002 on 3/31/2023."
    assert.equal(conflictDescription, email.conflict_description);
  });

  // Line 798 -> 806 -> 821 -> 827 -> 854 -> 862 -> 798 -> 924 -> 925 -> 958
  it('Email already exists in system and has been marked as sent, and change has occured. Follow up email required.', async function () {
    // Enter information for a single conflict into the database.
    let athlete = await db.collection("athletes").insertOne({
      "Student Email": "jacobj@msoe.edu",
      Name: "John Jacobs",
      Sport: "msoc"
    });
    let event = await db.collection("events").insertOne({
      Date: "3/31/2023",
      Opponent: "Oshkosh",
      Time: "12:00:00 PM",
      Location: "Oshkosh",
      "Departure Time": "8:00:00 AM",
      Sport: "msoc"
    });
    let classSchedule = await db.collection("student-schedules").insertOne({
      STUDENT_EMAIL: "jacobj@msoe.edu",
      YR_CDE: 2023,
      TRM_CDE: "Q3",
      CRS_CDE: "SE 3030 002",
      room_cde: "DH410",
      MONDAY_CDE: "",
      TUESDAY_CDE: "",
      WEDNESDAY_CDE: "W",
      THURSDAY_CDE: "",
      FRIDAY_CDE: "F",
      SATURDAY_CDE: "",
      BEGIN_TIM: "9:00:00 AM",
      END_TIM: "10:50:00 AM",
      BEGIN_DTE: "1/01/2000",
      END_DTE: "5/24/3000",
      Professor: "Yoder, Josiah",
      PROFESSOR_EMAIL: "yoder@msoe.edu"
    });

    // Generate the conflict and email for this data
    await indexMethods.createConflicts(db);
    await indexMethods.createEmails(db);

    // Update that email to be "sent"
    await db.collection("emails_to_send").findOneAndUpdate(
      {
        roster_id: ObjectId(athlete.insertedId),
        class_id: ObjectId(classSchedule.insertedId),
        event_id: ObjectId(event.insertedId)
      },
      { $set: { sent: true } }
    );

    // Make change to the departure time (1 hour earlier)
    await db.collection("events").findOneAndUpdate(
      {
        _id: event.insertedId
      },
      {
        $set: { "Departure Time": "7:00:00 AM" }
      }
    );

    // Recreate conflicts, then emails
    await indexMethods.createConflicts(db);
    await indexMethods.createEmails(db);

    // Assert that the email is marked as sent
    let email = await db.collection("emails_to_send").findOne({
      roster_id: ObjectId(athlete.insertedId),
      class_id: ObjectId(classSchedule.insertedId),
      event_id: ObjectId(event.insertedId)
    });
    assert.equal(true, email.sent);

    // Assert that the follow up email was sent
    assert.equal(true, fs.existsSync(path));
  });

  // Line 798 -> 806 -> 821 -> 827 -> 854 -> 858 -> 798 -> 924 -> 925 -> 958
  it('Email already exists in system and has been marked as sent, and a new professor has been assigned to the class.', async function () {
    // Enter information for a single conflict into the database.
    let athlete = await db.collection("athletes").insertOne({
      "Student Email": "jacobj@msoe.edu",
      Name: "John Jacobs",
      Sport: "msoc"
    });
    let event = await db.collection("events").insertOne({
      Date: "3/31/2023",
      Opponent: "Oshkosh",
      Time: "12:00:00 PM",
      Location: "Oshkosh",
      "Departure Time": "8:00:00 AM",
      Sport: "msoc"
    });
    let classSchedule = await db.collection("student-schedules").insertOne({
      STUDENT_EMAIL: "jacobj@msoe.edu",
      YR_CDE: 2023,
      TRM_CDE: "Q3",
      CRS_CDE: "SE 3030 002",
      room_cde: "DH410",
      MONDAY_CDE: "",
      TUESDAY_CDE: "",
      WEDNESDAY_CDE: "W",
      THURSDAY_CDE: "",
      FRIDAY_CDE: "F",
      SATURDAY_CDE: "",
      BEGIN_TIM: "9:00:00 AM",
      END_TIM: "10:50:00 AM",
      BEGIN_DTE: "1/01/2000",
      END_DTE: "5/24/3000",
      Professor: "Yoder, Josiah",
      PROFESSOR_EMAIL: "yoder@msoe.edu"
    });

    // Generate the conflict and email for this data
    await indexMethods.createConflicts(db);
    await indexMethods.createEmails(db);

    // Make change to the professor
    await db.collection("student-schedules").findOneAndUpdate(
      {
        _id: classSchedule.insertedId
      },
      {
        $set: {
          Professor: "Hasker, Robert",
          PROFESSOR_EMAIL: "hasker@msoe.edu"
        }
      }
    );

    // Update that email to be "sent"
    await db.collection("emails_to_send").findOneAndUpdate(
      {
        roster_id: ObjectId(athlete.insertedId),
        class_id: ObjectId(classSchedule.insertedId),
        event_id: ObjectId(event.insertedId)
      },
      { $set: { sent: true } }
    );

    // Recreate conflicts, then emails
    await indexMethods.createConflicts(db);
    await indexMethods.createEmails(db);

    // Assert that the email is marked as sent
    let email = await db.collection("emails_to_send").findOne({
      roster_id: ObjectId(athlete.insertedId),
      class_id: ObjectId(classSchedule.insertedId),
      event_id: ObjectId(event.insertedId)
    });
    assert.equal(true, email.sent);

    // Assert that the email's description has not changed.
    let conflictDescription = "John Jacobs has a departure time of 8:00:00 AM for a sports competition and may miss SE 3030 002 on 3/31/2023."
    assert.equal(conflictDescription, email.conflict_description);
  });

  // Line 798 -> 924 -> 925 -> 932 -> 934 -> 941 -> 925 -> 958
  it('No conflicts exists but 1 email exists that has been sent. A follow up email needed is needed', async function () {
    // Insert sent email
    let email = await db.collection("emails_to_send").insertOne({
      conflict_date: "3/31/2023",
      professor_email: "yoder@msoe.edu",
      professor_name: "Yoder, Josiah",
      conflict_description: "John Jacobs has a departure time of 8:00:00 AM for a sports competition and may miss SE 3030 002 on 3/31/2023.",
      disable_emails: false,
      sent: true
    });

    // Generate the conflict and email for this data
    await indexMethods.createConflicts(db);
    await indexMethods.createEmails(db);

    // Check that the follow up email has been sent
    assert.equal(true, fs.existsSync(path));

    // Assert that the email is no longer in the database
    let emailFound = await db.collection("emails_to_send").findOne({
      _id: ObjectId(email.insertedId)
    })
    assert.equal(null, emailFound);
  });

  // Line 798 -> 924 -> 925 -> 932 -> 947 -> 925 -> 958
  it('No conflicts exists but 1 email exists that has not been sent. Delete email', async function () {
    // Insert unsent email
    let email = await db.collection("emails_to_send").insertOne({
      conflict_date: "3/31/2023",
      professor_email: "yoder@msoe.edu",
      professor_name: "Yoder, Josiah",
      conflict_description: "John Jacobs has a departure time of 8:00:00 AM for a sports competition and may miss SE 3030 002 on 3/31/2023.",
      disable_emails: false,
      sent: false
    });

    // Generate the conflict and email for this data
    await indexMethods.createConflicts(db);
    await indexMethods.createEmails(db);

    // Assert that the email is no longer in the database
    let emailFound = await db.collection("emails_to_send").findOne({
      _id: ObjectId(email.insertedId)
    })
    assert.equal(null, emailFound);
  });
});