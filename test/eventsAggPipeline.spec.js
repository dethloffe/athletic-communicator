const mongoUnit = require('mongo-unit');
const MongoClient = require('mongodb').MongoClient;
const testData = require('./support/events_test_file.json');
const pipeline = require('../node_express_server/eventsAggPipeline.js');
const assert = require('chai').assert;

describe('Testing the eventsAggPipeline to assert that it is functioning properly', () => {
  let db;

  beforeEach(() => {
    mongoUnit.load(testData);
    const client = new MongoClient(mongoUnit.getUrl(), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = client.db("test");
  });

  afterEach(() => {
    mongoUnit.drop();
  })

  it('If a sports event is uploaded with the date "12/05/2022" and no departure time the pipeline will set the field formatted_full_datetime to null', async function() {
    const testCursor = db.collection("events").aggregate(pipeline);
    await testCursor.toArray();
    const entry = await db.collection("events").findOne({_id: "0"}).catch(err => console.log("Error finding db entry:\n", err));

    // formatted_date should contain date with no time as example file has no departure time
    assert.isNull(entry.formatted_full_datetime);
  });

  it('If a sports event is uploaded with the date 9/17/2022 and a departure time of 3:30:00 PM then the formatted_full_datetime should be 2022-09-17T15:30:00.000Z', async function() {
    const testCursor = db.collection("events").aggregate(pipeline);
    await testCursor.toArray();
    const entry = await db.collection("events").findOne({_id: "1"}).catch(err => console.log("Error finding db entry:\n", err));

    let dateTime = new Date('2022-09-17T15:30:00.000Z');
    let date = new Date('2022-09-17T00:00:00.000Z');

    assert.equal(+dateTime, +entry.formatted_full_datetime);
    assert.equal(+date, +entry.formatted_date);
  });

  it('If a sports event is uploaded with the date 9/18/2022 then the day, dayOfMonth, month, and year should should all be accurate to that date.', async function() {
  const testCursor = db.collection("events").aggregate(pipeline);
  await testCursor.toArray();
  const entry = await db.collection("events").findOne({_id: "2"}).catch(err => console.log("Error finding db entry:\n", err));

  assert.equal(1, entry.day);
  assert.equal(18, entry.dayOfMonth);
  assert.equal(9, entry.month);
  assert.equal(2022, entry.year);
  });

  it('If an invalid date is entered in any format other than the accepted MM/DD/YYY then the pipeline will set the full_formatted_datetime to null.', async function() {
    const testCursor = db.collection("events").aggregate(pipeline);
    await testCursor.toArray();

    const dashes = await db.collection("events").findOne({_id: "3"}).catch(err => console.log("Error finding db entry:\n", err));
    const spaces = await db.collection("events").findOne({_id: "4"}).catch(err => console.log("Error finding db entry:\n", err));
    const tbd = await db.collection("events").findOne({_id: "5"}).catch(err => console.log("Error finding db entry:\n", err));
    const bigMonth = await db.collection("events").findOne({_id: "6"}).catch(err => console.log("Error finding db entry:\n", err));
    const bigDay = await db.collection("events").findOne({_id: "7"}).catch(err => console.log("Error finding db entry:\n", err));
    const bigMonthDay = await db.collection("events").findOne({_id: "8"}).catch(err => console.log("Error finding db entry:\n", err));

    assert.isNull(dashes.formatted_full_datetime);
    assert.isNull(spaces.formatted_full_datetime);
    assert.isNull(tbd.formatted_full_datetime);
    assert.isNull(bigMonth.formatted_full_datetime);
    assert.isNull(bigDay.formatted_full_datetime);
    assert.isNull(bigMonthDay.formatted_full_datetime);
    });
});
