const initialLateEmailRequired = require('../node_express_server/index.js');
const testData = require('./support/lateInitialEmailData.json');
const assert = require('chai').assert;

describe('Testing the initialLateEmailRequired method to ensure it properly determines the time period in which an initial late email is required.', () => {
    testData.times.forEach(function (data) {
        it(data.description, function () {
            assert.equal(initialLateEmailRequired.lateInitialEmailRequired(data.conflictDate, data.today), data.expectedOutput);
        });
    });
});