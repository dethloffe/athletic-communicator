const mongoUnit = require('mongo-unit');
const MongoClient = require('mongodb').MongoClient;
const testData = require('./support/daylights-savings-test.json');
const pipeline = require('../node_express_server/eventsAggPipeline.js');
const assert = require('chai').assert;

describe('Testing the daylight savings functionality of mongoDB', () => {
  let db;

  beforeEach(() => {
    mongoUnit.load(testData);
    const client = new MongoClient(mongoUnit.getUrl(), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = client.db("test");
  });

  afterEach(() => {
    mongoUnit.drop();
  })

  it('If two events are uploaded one with a date right before DST and right after DST with the same time, 3:00:00 PM should be set to the same time on UTC default.', async function() {
    const testCursor = db.collection("events").aggregate(pipeline);
    await testCursor.toArray();
    const edgewood = await db.collection("events").findOne({_id: "0"}).catch(err => console.log("Error finding db entry:\n", err));
    const elmhurst = await db.collection("events").findOne({_id: "1"}).catch(err => console.log("Error finding db entry:\n", err));

    let edDate = new Date('2023-03-12T15:00:00.000Z');
    let elDate = new Date('2023-03-13T15:00:00.000Z');

    // Because the 13th of march is after DST and the 12th is during DST the hour in UTC is differnt even when setting the hour to the same thing in CST
    // It appears as though even though DST ends at 2:00 AM on march 12th mongoDB and javaScript treats all of March 12th as being part of DST
    assert.equal(+edDate, +edgewood.formatted_full_datetime, "dates should be equal");
    assert.equal(+elDate, +elmhurst.formatted_full_datetime, "dates should be equal");
  });

  it('If the date is March 12th and the time is 8:30 AM the UTC time will be 8:30 AM.', async function() {
    const testCursor = db.collection("events").aggregate(pipeline);
    await testCursor.toArray();
    const event = await db.collection("events").findOne({_id: "2"}).catch(err => console.log("Error finding db entry:\n", err));

    let date = new Date('2023-03-14T08:30:00.000Z');

    // Even though 2:30 AM does not exist on March 12th 2023 the Date object in javascript is set to 8:30 AM because it does not recognize DST until March 13th
    assert.equal(+date, +event.formatted_full_datetime, "dates should be equal");
  });
});
