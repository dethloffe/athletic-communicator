const mongoUnit = require('mongo-unit');
const MongoClient = require('mongodb').MongoClient;
const rosterData = require('./support/athletes-test.json');

const eventPipe = require('../node_express_server/eventsAggPipeline.js');
const schedulePipe = require('../node_express_server/studentSchedulesAggPipeline.js');
const conflictPipe = require('../node_express_server/conflictsAggPipeline.js');
const assert = require('chai').assert;

describe('Testing the conflictsAggPipeline to assure that the correct conflicts are being created', () => {
  let db;

  beforeEach(() => {
    mongoUnit.load(rosterData);
    const client = new MongoClient(mongoUnit.getUrl(), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = client.db("test");
  });

  afterEach(() => {
    mongoUnit.drop();
  })

  it('If the test data is uploaded as given then three conflicts should be created', async function() {
    const eventCursor = db.collection("events").aggregate(eventPipe);
    await eventCursor.toArray();
    const scheduleCursor = db.collection("student-schedules").aggregate(schedulePipe);
    await scheduleCursor.toArray();
    const conflicts = db.collection("student-schedules").aggregate(conflictPipe);
    await conflicts.toArray();
    
    const num = await db.collection("conflicts").countDocuments();

    assert.equal(3, num);
  });

  it('If an event occurs on a date after the end date of a class then no conflict should be created', async function() {
    const eventCursor = db.collection("events").aggregate(eventPipe);
    await eventCursor.toArray();
    const scheduleCursor = db.collection("student-schedules").aggregate(schedulePipe);
    await scheduleCursor.toArray();
    const conflicts = db.collection("student-schedules").aggregate(conflictPipe);
    await conflicts.toArray();

    const conflict = await db.collection("conflicts").findOne({date_of_conflict:"11/21/2022"}).catch(err => console.log("Error locating conflict: ", err));

    assert.isNull(conflict);
  });

  it('If an event occurs on a date before the start date of a class then no conflict should be created', async function() {
    const eventCursor = db.collection("events").aggregate(eventPipe);
    await eventCursor.toArray();
    const scheduleCursor = db.collection("student-schedules").aggregate(schedulePipe);
    await scheduleCursor.toArray();
    const conflicts = db.collection("student-schedules").aggregate(conflictPipe);
    await conflicts.toArray();

    const conflict = await db.collection("conflicts").findOne({date_of_conflict:"09/02/2022"}).catch(err => console.log("Error locating conflict: ", err));

    assert.isNull(conflict);
  });

  it('If the test data is uploaded and run through the correct pipelines then the following conflict should be created', async function() {
    const eventCursor = db.collection("events").aggregate(eventPipe);
    await eventCursor.toArray();
    const scheduleCursor = db.collection("student-schedules").aggregate(schedulePipe);
    await scheduleCursor.toArray();
    const conflicts = db.collection("student-schedules").aggregate(conflictPipe);
    await conflicts.toArray();

    const conflict = await db.collection("conflicts").findOne({date_of_conflict:"11/07/2022"}).catch(err => console.log("Error locating conflict: ", err));

    assert.equal('John Jacobs', conflict.athlete_name);
    assert.equal('wsoc', conflict.athlete_sport);
    assert.equal('McAninch, Andrew', conflict.professor);
    assert.equal('HU432003', conflict.course);
    assert.equal('10:00:00 AM', conflict.course_begin_time);
    assert.equal('10:50:00 AM', conflict.course_end_time);
    assert.equal('11/07/2022', conflict.date_of_conflict);
    assert.equal('7:00:00 AM', conflict.departure_time);
    assert.equal('pastenb@msoe.edu', conflict.professor_email);
    assert.isTrue(conflict.sports_match);
    assert.isTrue(conflict.overlaps_with_class);
    assert.isFalse(conflict.after_end_date);
    assert.isFalse(conflict.before_start_date);
  });
});
