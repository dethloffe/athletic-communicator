const mongoUnit = require('mongo-unit');
const MongoClient = require('mongodb').MongoClient;
const testData = require('./support/exam-schedule-test-file.json');
const pipeline = require('../node_express_server/finalAggPipeline.js');
const assert = require('chai').assert;

describe('Testing the finalAggPipeline to assert that it is functioning properly', () => {
  let db;

  beforeEach(() => {
    mongoUnit.load(testData);
    const client = new MongoClient(mongoUnit.getUrl(), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = client.db("test");
  });

  afterEach(() => {
    mongoUnit.drop();
  })

  it('If a exam is uploaded with the EXAM_TIME of 10:00 AM ‐ 12:00 PM then the start_time should be 10:00 AM', async function() {
    const testCursor = db.collection("exam-schedule").aggregate(pipeline);
    await testCursor.toArray();
    const entry = await db.collection("exam-schedule").findOne({_id: "0"}).catch(err => console.log("Error finding db entry:\n", err));

    assert.equal('10:00 AM', entry.start_time);
  });

  it('If an exam is uploaded with the EXAM_DAY 12/6/2022 and the EXAM_TIME of 8:00 AM ‐ 10:00 AM then the datetime should be 12/6/2022 8:00 AM', async function() {
    const testCursor = db.collection("exam-schedule").aggregate(pipeline);
    await testCursor.toArray();
    const entry = await db.collection("exam-schedule").findOne({_id: "1"}).catch(err => console.log("Error finding db entry:\n", err));

    assert.equal('12/6/2022 8:00 AM ', entry.datetime);
   });

   it('If an exam is uploaded with COURSE: BI 102 012 and the datetime is 12/6/2022 8:00 AM then the course_code should be BI102012 and the formatted_date should be 2022-12-06T08:00:00.000Z', async function() {
    const testCursor = db.collection("exam-schedule").aggregate(pipeline);
    await testCursor.toArray();
    const entry = await db.collection("exam-schedule").findOne({_id: "2"}).catch(err => console.log("Error finding db entry:\n", err));

    let date = new Date('2022-12-06T08:00:00.000Z');

    assert.equal(+date, +entry.formatted_date);
    assert.equal('BI102012', entry.course_code);
   });

   it('If an exam is uploaded with the date 12/6/2022 then the dayOfMonth, month, and year should should all be accurate to that date.', async function() {
    const testCursor = db.collection("exam-schedule").aggregate(pipeline);
    await testCursor.toArray();
    const entry = await db.collection("exam-schedule").findOne({_id: "2"}).catch(err => console.log("Error finding db entry:\n", err));

    assert.equal(6, entry.dayOfMonth);
    assert.equal(12, entry.month);
    assert.equal(2022, entry.year);
   });

   it('If an invalid date is entered in any format other than the accepted MM/DD/YYY then the pipeline will set the full_formatted_datetime to null.', async function() {
    const testCursor = db.collection("exam-schedule").aggregate(pipeline);
    await testCursor.toArray();

    const spaces = await db.collection("exam-schedule").findOne({_id: "4"}).catch(err => console.log("Error finding db entry:\n", err));
    const tbd = await db.collection("exam-schedule").findOne({_id: "5"}).catch(err => console.log("Error finding db entry:\n", err));
    const bigMonth = await db.collection("exam-schedule").findOne({_id: "6"}).catch(err => console.log("Error finding db entry:\n", err));
    const bigDay = await db.collection("exam-schedule").findOne({_id: "7"}).catch(err => console.log("Error finding db entry:\n", err));
    const bigMonthDay = await db.collection("exam-schedule").findOne({_id: "8"}).catch(err => console.log("Error finding db entry:\n", err));

    assert.isNull(spaces.formatted_date);
    assert.isNull(tbd.formatted_date);
    assert.isNull(bigMonth.formatted_date);
    assert.isNull(bigDay.formatted_date);
    assert.isNull(bigMonthDay.formatted_date);
    });
});