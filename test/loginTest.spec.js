const login = require('../nuxt_website/api/processLogin.js');
const testData = require('./support/login-data.json');
const assert = require('chai').assert;

describe(
    'Testing the login function method to ensure that it properly returns',
    () => {
    testData.items.forEach(function (data) {
        it(data._id, function () {
            assert.equal(login.checkLogin({
                username: data.username,
                password: data.password
            }), data.expectedResult);
        });
    });
});