const mongoUnit = require('mongo-unit');
const MongoClient = require('mongodb').MongoClient;
const assert = require('chai').assert;
const indexMethods = require('../node_express_server/index.js');
const { ObjectId } = require('mongodb');
const fs = require('fs');
const path = 'C:/Projects/Athcomm/athcomm/newfile.txt';

describe('Testing the sendEmails() method in node_express_server/index.js', () => {
  let db;

  beforeEach(async () => {
    const client = new MongoClient(mongoUnit.getUrl(), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    db = client.db("athcomm");
    await db.collection("athletes").deleteMany({});
    await db.collection("events").deleteMany({});
    await db.collection("exam-schedule").deleteMany({});
    await db.collection("student-schedules").deleteMany({});
    await db.collection("conflicts").deleteMany({});
    await db.collection("emails_to_send").deleteMany({});
  });

  afterEach(() => {
    mongoUnit.drop();

    if (fs.existsSync(path)) {
      fs.unlinkSync(path);
    }
  })

  // 997 -> 998 -> 999 -> 1000 -> 1028 -> 1030 -> 1031 -> 1039 -> 1041 -> 1039 -> 1077
  it('Sending an initial email is on, there is a email that is not disabled, not sent, and requires an initial late email.', async function () {
    // Insert information for a single email that requires a late initial email.
    let athlete = await db.collection("athletes").insertOne({
      "Student Email": "jacobj@msoe.edu",
      Name: "John Jacobs",
      Sport: "msoc"
    });
    let event = await db.collection("events").insertOne({
      Date: "4/15/2023",
      Opponent: "Oshkosh",
      Time: "12:00:00 PM",
      Location: "Oshkosh",
      "Departure Time": "8:00:00 AM",
      Sport: "msoc"
    });
    let classSchedule = await db.collection("student-schedules").insertOne({
      STUDENT_EMAIL: "jacobj@msoe.edu",
      YR_CDE: 2023,
      TRM_CDE: "Q3",
      CRS_CDE: "SE 3030 002",
      room_cde: "DH410",
      MONDAY_CDE: "",
      TUESDAY_CDE: "",
      WEDNESDAY_CDE: "W",
      THURSDAY_CDE: "",
      FRIDAY_CDE: "F",
      SATURDAY_CDE: "S",
      BEGIN_TIM: "9:00:00 AM",
      END_TIM: "10:50:00 AM",
      BEGIN_DTE: "1/01/2000",
      END_DTE: "5/24/3000",
      Professor: "Yoder, Josiah",
      PROFESSOR_EMAIL: "yoder@msoe.edu"
    });

    // Generate the conflict and email for this data
    await indexMethods.createConflicts(db);
    await indexMethods.createEmails(db);

    // Call the send emails method
    await indexMethods.sendEmails(false, true, db, true);

    // Assert that the email is marked as sent
    let email = await db.collection("emails_to_send").findOne({
      roster_id: ObjectId(athlete.insertedId),
      class_id: ObjectId(classSchedule.insertedId),
      event_id: ObjectId(event.insertedId)
    });
    assert.equal(email.sent, true);

    // Assert that the email was actually sent
    assert.equal(fs.existsSync(path), true);
  });

  // 997 -> 1007 -> 1009 -> 1011 -> 1012 -> 1028 -> 1030 -> 1034 -> 1039 -> 1060 -> 1028 -> 1077
  it('Send initial is off, there is a single email in the database. Date of the email makes it a near future conflict.', async function () {
    // Enter information for a single conflict into the database.
    let athlete = await db.collection("athletes").insertOne({
      "Student Email": "jacobj@msoe.edu",
      Name: "John Jacobs",
      Sport: "msoc"
    });
    let event = await db.collection("events").insertOne({
      Date: "4/21/2023",
      Opponent: "Oshkosh",
      Time: "12:00:00 PM",
      Location: "Oshkosh",
      "Departure Time": "8:00:00 AM",
      Sport: "msoc"
    });
    let classSchedule = await db.collection("student-schedules").insertOne({
      STUDENT_EMAIL: "jacobj@msoe.edu",
      YR_CDE: 2023,
      TRM_CDE: "Q3",
      CRS_CDE: "SE 3030 002",
      room_cde: "DH410",
      MONDAY_CDE: "",
      TUESDAY_CDE: "",
      WEDNESDAY_CDE: "W",
      THURSDAY_CDE: "",
      FRIDAY_CDE: "F",
      SATURDAY_CDE: "",
      BEGIN_TIM: "9:00:00 AM",
      END_TIM: "10:50:00 AM",
      BEGIN_DTE: "1/01/2000",
      END_DTE: "5/24/3000",
      Professor: "Yoder, Josiah",
      PROFESSOR_EMAIL: "yoder@msoe.edu"
    });

    // Generate the conflict and email for this data
    await indexMethods.createConflicts(db);
    await indexMethods.createEmails(db);

    // Call the send emails method
    await indexMethods.sendEmails(false, false, db, true);

    // Assert that the email is marked as sent
    let email = await db.collection("emails_to_send").findOne({
      roster_id: ObjectId(athlete.insertedId),
      class_id: ObjectId(classSchedule.insertedId),
      event_id: ObjectId(event.insertedId)
    });
    assert.equal(email.sent, true);

    // Assert that the email was actually sent
    assert.equal(fs.existsSync(path), true);
  });

  // 997 -> 1007 -> 1028 -> 1077
  it('No data exists in the database', async function () {
    // Generate the conflict and email
    await indexMethods.createConflicts(db);
    await indexMethods.createEmails(db);

    // Call the send emails method
    await indexMethods.sendEmails(false, false, db, true);

    // Confirm no emails are in the system
    let emails = await db.collection("emails_to_send").find({}).toArray();
    assert.equal(emails.length, 0);

    // Confirm no conflicts are in the system
    let conflicts = await db.collection("conflicts").find({}).toArray();
    assert.equal(conflicts.length, 0);

    // Confirm that no emails were sent
    assert.equal(fs.existsSync(path), false);
  });

  // 997 -> 998 -> 999 -> 1000 -> 998 -> 1028 -> 1030 -> 1031 -> 1039 -> 1041 -> 1042 -> 1039 -> 1060 -> 1028 -> 1077
  it('Send initial is on, two near future conflict exists in the database for the same professor.', async function () {
    // Insert two near future conflicts into the database.
    let athlete1 = await db.collection("athletes").insertOne({
      "Student Email": "jacobj@msoe.edu",
      Name: "John Jacobs",
      Sport: "msoc"
    });
    let athlete2 = await db.collection("athletes").insertOne({
      "Student Email": "jiminyc@msoe.edu",
      Name: "Jiminy Cricket",
      Sport: "msoc"
    });
    let event = await db.collection("events").insertOne({
      Date: "4/15/2023",
      Opponent: "Oshkosh",
      Time: "12:00:00 PM",
      Location: "Oshkosh",
      "Departure Time": "8:00:00 AM",
      Sport: "msoc"
    });
    let classSchedule1 = await db.collection("student-schedules").insertOne({
      STUDENT_EMAIL: "jacobj@msoe.edu",
      YR_CDE: 2023,
      TRM_CDE: "Q3",
      CRS_CDE: "SE 3030 002",
      room_cde: "DH410",
      MONDAY_CDE: "",
      TUESDAY_CDE: "",
      WEDNESDAY_CDE: "W",
      THURSDAY_CDE: "",
      FRIDAY_CDE: "F",
      SATURDAY_CDE: "S",
      BEGIN_TIM: "9:00:00 AM",
      END_TIM: "10:50:00 AM",
      BEGIN_DTE: "1/01/2000",
      END_DTE: "5/24/3000",
      Professor: "Yoder, Josiah",
      PROFESSOR_EMAIL: "yoder@msoe.edu"
    });
    let classSchedule2 = await db.collection("student-schedules").insertOne({
      STUDENT_EMAIL: "jiminyc@msoe.edu",
      YR_CDE: 2023,
      TRM_CDE: "Q3",
      CRS_CDE: "SE 3030 002",
      room_cde: "DH410",
      MONDAY_CDE: "",
      TUESDAY_CDE: "",
      WEDNESDAY_CDE: "W",
      THURSDAY_CDE: "",
      FRIDAY_CDE: "F",
      SATURDAY_CDE: "S",
      BEGIN_TIM: "9:00:00 AM",
      END_TIM: "10:50:00 AM",
      BEGIN_DTE: "1/01/2000",
      END_DTE: "5/24/3000",
      Professor: "Yoder, Josiah",
      PROFESSOR_EMAIL: "yoder@msoe.edu"
    });

    // Create conflicts and emails
    await indexMethods.createConflicts(db);
    await indexMethods.createEmails(db);

    // Call the send emails method
    await indexMethods.sendEmails(false, true, db, true);

    // Confirm both emails are marked as sent
    let email1 = await db.collection("emails_to_send").findOne({
      roster_id: ObjectId(athlete1.insertedId),
      class_id: ObjectId(classSchedule1.insertedId),
      event_id: ObjectId(event.insertedId)
    });
    let email2 = await db.collection("emails_to_send").findOne({
      roster_id: ObjectId(athlete2.insertedId),
      class_id: ObjectId(classSchedule2.insertedId),
      event_id: ObjectId(event.insertedId)
    });
    assert.equal(email1.sent, true);
    assert.equal(email2.sent, true);

    // Assert that the email actually sent
    assert.equal(fs.existsSync(path), true);
  });
});