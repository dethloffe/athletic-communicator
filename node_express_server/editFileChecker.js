module.exports = {
    checkEdits, checkDate, hasNumbers
}

let letters = /^[A-Za-z]+$/;

async function checkEdits(editedData, collectionName) {
    let response;
    let examDays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    let examDayExceptions = ["No Scheduled Final Exam", "See Faculty", "Online"];

    switch (collectionName) {
        case "athletes":
            // Student email
            response = await checkEmail(editedData["Student Email"]);
            if (response !== 0) {
                return response;
            }

            // Sport
            response = await checkSport(editedData.Sport);
            if (response !== 0) {
                return response;
            }
            break;
        case "events":
            // Date
            response = await checkDate(editedData.Date, collectionName);
            if (response != 0) {
                return response;
            }

            // Time
            response = await checkTime(editedData.Time);
            if (response !== 0) {
                return response;
            }
            response = await checkTime(editedData["Departure Time"]);
            if (response !== 0) {
                return response;
            }

            // Sport
            response = await checkSport(editedData.Sport);
            if (response !== 0) {
                return response
            }
            break;
        case "student-schedules":
            // Professor name
            response = await checkProfessorName(editedData.Professor);
            if (response !== 0) {
                return response;
            }

            // Student email
            response = await checkEmail(editedData.STUDENT_EMAIL);
            if (response !== 0) {
                return response;
            }

            // Year Code
            let thisYear = new Date().getFullYear();
            if (parseInt(editedData.YR_CDE) < (thisYear - 1)) {
                return "The Year Code of '" + editedData.YR_CDE + "' has been determined to be in the past."
            }

            // Begin Date
            response = await checkDate(editedData.BEGIN_DTE);
            if (response != 0) {
                return response;
            }
            
            // End Date
            response = await checkDate(editedData.END_DTE);
            if (response != 0) {
                return response;
            }

            // Begin Time
            response = await checkTime(editedData.BEGIN_TIM);
            if (response !== 0) {
                return response;
            }

            // End Time
            response = await checkTime(editedData.END_TIM);
            if (response !== 0) {
                return response;
            }

            // Professor email
            response = await checkEmail(editedData.PROFESSOR_EMAIL);
            if (response !== 0) {
                return response;
            }
            break;
        case "exam-schedule":
            // Professor Name
            response = await checkProfessorName(editedData.INSTRUCTOR);
            if (response !== 0) {
                return response;
            }

            // Exam Day
            if (!examDayExceptions.includes(editedData.EXAM_DAY)) {
                if (!examDays.includes(editedData.EXAM_DAY)) {
                    return "The Exam Day: '" + editedData.EXAM_DAY + "' is not one of the following: Monday, Tuesday, Wednesday, Thursday, Friday, or Saturday. Please update this entry.";
                }
                
                // Exam Time
                response = await checkExamTime(editedData.EXAM_TIME);
                if (response !== 0) {
                    return response;
                }
            }
            break;
    }

    return 0;
}

async function checkTime(entry) {
    if (entry.trim() == "") {
        return 0;
    }
    try {
        let time = entry.toString().split(":");
        let hours = parseInt(time[0]);
        let minutes = parseInt(time[1]);
        let seconds = parseInt(time[2].split(" ")[0]);
        let meridiem = time[2].split(" ")[1];

        if (hours.toString() == "NaN" || hours > 12 || hours < 1) {
            return "The time of '" + entry + "' is not in the format HH:MM:SS AM/PM. The hour has been determined to be incorrect.";
        }
        if (minutes.toString() == "NaN" || minutes > 59 || minutes < 0) {
            return "The time of '" + entry + "' is not in the format HH:MM:SS AM/PM. The minutes have been determined to be incorrect.";
        }
        if (seconds.toString() == "NaN" || seconds > 59 || seconds < 0) {
            return "The time of '" + entry + "' is not in the format HH:MM:SS AM/PM. The seconds have been determined to be incorrect.";
        }
        if (meridiem !== "AM" && meridiem !== "PM") {
            return "The time of '" + entry + "' is not in the format HH:MM:SS AM/PM. The meridiem (AM/PM) has been determined to be incorrect.";
        }
        if (time[2].split(" ")[2]) {
            return "Unnessecary information: '" + time[2].split(" ")[2] + "' was found to be tacked on after the HH:MM:SS AM/PM."
        }
    } catch {
        return "The time of '" + entry + "' is not in the format HH:MM:SS AM/PM.";
    }

    return 0;
}

async function checkDate(entry, collectionName) {
    if (entry.trim() == "") {
        return 0;
    }
    try {
        let date = entry.toString().split("/");
        let month = parseInt(date[0]);
        let day = parseInt(date[1]);
        let year = parseInt(date[2]);
        let dateObject = new Date(entry);
        let today = new Date();

        if (month.toString() == "NaN" || month > 12 || month < 1) {
            return "The date of '" + entry + "' is not in the format MM/DD/YYYY. The month has been determined to be incorrect."
        }
        if (
            // Day is not a number
            day.toString() == "NaN" ||
            // Months with 28 days (February non-leap year)
            ((year % 4 != 0) && (month == 2) && (day > 28 || day < 1)) ||
            // Months with 29 days (February leap year)
            ((year % 4 == 0) && (month == 2) && (day > 29 || day < 1)) ||
            // Months with 30 days
            ((month == 4 || month == 6 || month == 9 || month == 11) && (day > 30 || day < 1)) ||
            // Months with 31 days
            ((month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) && (day > 31 || day < 1))
        ) {
            return "The date of '" + entry + "' is not in the format MM/DD/YYYY. The day has been determined to be incorrect."
        }
        if (dateObject.getTime() < today.getTime() && collectionName === "events" && dateObject.toDateString() !== today.toDateString()) {
            return "The date of '" + entry + "' has been determined to be in the past."
        }
        if (year.toString() == "NaN") {
            return "The date of '" + entry + "' is not in the format MM/DD/YYYY. The year has been determined to be incorrect."
        }
        if (date[3]) {
            return "Unnessecary information: '" + date[3] + "' was found to be tacked on after the MM/DD/YYYY."
        }
    } catch {
        return "The date of '" + entry + "' is not in the format MM/DD/YYYY.";
    }

    return 0;
}

async function checkExamTime(entry) {
    if (entry.trim() == "") {
        return 0;
    }
    try {
        let fullEntry = entry;
        entry = entry.trim();
        let startHour = parseInt(entry.toString().substring(0, entry.toString().indexOf(':')));
        let startMeridiem = entry.toString().substring(entry.toString().indexOf(' ') + 1, entry.toString().indexOf(' ') + 3);
        let startMinutes = parseInt(entry.toString().substring(entry.toString().indexOf(':') + 1, entry.toString().indexOf(' ')));
        entry = entry.toString().substring(entry.toString().indexOf(startMeridiem) + 4).trim();
        let endHour = parseInt(entry.toString().substring(0, entry.toString().indexOf(':')));
        let endMeridiem = entry.toString().substring(entry.toString().indexOf(' ') + 1, entry.toString().indexOf(' ') + 3);
        let endMinutes = parseInt(entry.toString().substring(entry.toString().indexOf(':') + 1, entry.toString().indexOf(' ')));

        // Meridiems
        if (startMeridiem !== "AM" && startMeridiem !== "PM") {
            return "Your exam start time meridiem for: '" + fullEntry + "' is neither AM nor PM. Please make sure the time format is 'HH:MM AM/PM - HH:MM AM/PM'.";
        }
        if (endMeridiem !== "AM" && endMeridiem !== "PM") {
            return "Your exam end time meridiem for: '" + fullEntry + "' is neither AM nor PM. Please make sure the time format is 'HH:MM AM/PM - HH:MM AM/PM'.";
        }
        if (startMeridiem == "PM" && endMeridiem == "AM") {
            return "Your end time meridiem for: '" + fullEntry + "' is AM and your start time meridiem is PM. These contradict. Please check your Exam Times.";
        }

        // Hours
        if (startHour.toString() == "NaN" || startHour > 12 || startHour < 1) {
            return "The exam time of '" + fullEntry + "' is not in the format 'HH:MM AM/PM - HH:MM AM/PM'. The start hour has been determined to be incorrect.";
        }
        if (endHour.toString() == "NaN" || endHour > 12 || endHour < 1) {
            return "The exam time of '" + fullEntry + "' is not in the format 'HH:MM AM/PM - HH:MM AM/PM'. The start hour has been determined to be incorrect.";
        }

        // Minutes
        if (startMinutes.toString() == "NaN" || startMinutes > 59 || startMinutes < 0) {
            return "The exam time of '" + fullEntry + "' is not in the format 'HH:MM AM/PM - HH:MM AM/PM'. The start time minutes have been determined to be incorrect.";
        }
        if (endMinutes.toString() == "NaN" || endMinutes > 59 || endMinutes < 0) {
            return "The exam time of '" + fullEntry + "' is not in the format 'HH:MM AM/PM - HH:MM AM/PM'. The end time minutes have been determined to be incorrect.";
        }

        // Within exam hours of 8am to 10pm
        if (startMeridiem == "AM" && startHour < 8) {
            return "The exam time of '" + fullEntry + "' is not within the exam hours of 8am to 10pm. This time has been detcted to start before 8 am."
        }
        if (endMeridiem == "PM" && endHour > 10) {
            return "The exam time of '" + fullEntry + "' is not within the exam hours of 8am to 10pm. This time has been detcted to end after 10 pm."
        }
    } catch {
        return "The Exam Time of '" + fullEntry + "' is not in the format 'HH:MM AM/PM - HH:MM AM/PM'.";
    }
    return 0;
}

async function checkEmail(email) {
    if (email.toString().includes(' ')) {
        return "The email: '" + email + "' has been found to include a space. There should be no space within this entry.";
    }

    if (
        email.toString().toLowerCase().includes('@') &&
        email.toString().toLowerCase().substring(email.toString().indexOf('@')) !== "@msoe.edu"
    ) {
        return "The email: '" + email + "' does not end in the expected '@msoe.edu'. Make sure this is a proper msoe email.";
    }

    return 0;
}

async function checkSport(sport) {
    if (sport === undefined || sport.toString().trim() === "") {
        return "No sport was detected."
    } else if (!sport.toString().match(letters)) {
        // Sport contains characters other than letters. This infers there is an incorrect entry.
        return "Sport of '" + sport + "' was detected as an incorrect sport code.";
    } else {
        return 0;
    }
}

async function checkProfessorName(professorName) {
    if (
        professorName.indexOf(',') == -1 ||
        professorName.substring(professorName.indexOf(',')).trim() == "" ||
        professorName.substring(0, professorName.indexOf(',')).trim() == ""
    ) {
        return "The Professor name: '" + professorName + "' has been determined to be in incorrect form. Correct format: 'Lastname, Firstname'";
    } else {
        return 0;
    }
}

function hasNumbers(entry) {
    return /\d/.test(entry);
}