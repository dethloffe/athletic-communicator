module.exports = {
  lateInitialEmailRequired,
  sendEmails,
  createEmails,
  createConflicts
}

// import required files
const { ObjectId } = require('mongodb');
const express = require("express");
const util = require('util');
const axios = require("axios");
const assert = require("assert");
const fs = require('fs');
const eventAggPipeline = require("./eventsAggPipeline")
const studentSchedulesAggPipeline = require("./studentSchedulesAggPipeline")
const rosterAggPipeline = require("./rosterAggPipeline")
const conflictsAggPipeline = require("./conflictsAggPipeline")
const finalAggPipeline = require("./finalAggPipeline")
const finalConflictAggPipeline = require("./finalConflictAggPipline")

// set up utilities/constants
const exec = util.promisify(require('child_process').exec);
const MongoClient = require("mongodb").MongoClient;
const crypto = require('crypto')
const app = express();
const port = 4000;
let url;
let mongodbConnectionStatus = false;

if (process.env.LOCAL == 1) {
  url = "mongodb://host.docker.internal:27017";
} else {
  url = "mongodb://localhost:27017";
}

const client = new MongoClient(url, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// Connect to MongoDB server
client.connect(function (err) {
  if (err) {
    console.error("error connecting to MongoDB server on port 27017, exiting node express server process...")
    mongodbConnectionStatus = false;
    // process.exit(0);
  }
  assert.equal(null, err);
  console.log("[Node_Server] Connected successfully to MongoDB server on port 27017");
  mongodbConnectionStatus = true
});

dbName = "athcomm";
const db = client.db(dbName);

async function mongodbPing() {
  try {
    await client.db("admin").command({ ping: 1 });
    return true
  } catch (err) {
    console.log(err)
    return false
  }
}

app.listen(port, () => console.log("[Node_Server] athcomm node express server listening on port " + port));

app.get("/", async (req, res) => res.json({ nodeExpressStatus: true, mongoStatus: await mongodbPing() }));

/*-----------------------------------------------------------------------
     __                          __   _   _          _         
    / /   ___    ___    _ __    / _| | | (_)   ___  | |_   ___ 
   / /   / __|  / _ \  | '_ \  | |_  | | | |  / __| | __| / __|
  / /   | (__  | (_) | | | | | |  _| | | | | | (__  | |_  \__ \
 /_/     \___|  \___/  |_| |_| |_|   |_| |_|  \___|  \__| |___/
                                                               
-----------------------------------------------------------------------*/
app.get("/conflicts", async (req, res) => {
  try {
    console.log("[Node_Server] doing conflict things on node express server")

    console.log("[Node_Server] attempting to get conflicts")
    const aggCursor4 = db.collection("conflicts").find({ sports_match: true });

    const result = []

    for await (const doc of aggCursor4) {
      result.push(doc)
    }

    console.log("[Node_Server] got conflicts: ", result)

    res.send(result);
  } catch (error) {
    res.send({ status: 500, data: "Error gathering conflicts" });
  }
});

/*-----------------------------------------------------------------------
      __          _          _                    _____          _         
     / /         | |        (_)                  |  __ \        | |        
    / /_ __  ___ | |_  _ __  _   ___ __   __ ___ | |  | |  __ _ | |_  __ _ 
   / /| '__|/ _ \| __|| '__|| | / _ \\ \ / // _ \| |  | | / _` || __|/ _` |
  / / | |  |  __/| |_ | |   | ||  __/ \ V /|  __/| |__| || (_| || |_| (_| |
 /_/  |_|   \___| \__||_|   |_| \___|  \_/  \___||_____/  \__,_| \__|\__,_|
-----------------------------------------------------------------------*/
app.get("/retrieveData", async (req, res) => {
  try {
    let dataWanted = req.query.dataWanted;
    const aggCursor4 = await db.collection(dataWanted).find({});

    const result = [];

    for await (const doc of aggCursor4) {
      result.push(doc);
    }

    console.log("[Node_Server] got ", dataWanted);

    res.send(result);
  } catch (error) {
    res.send({ status: 500, data: "Error gathering ", dataWanted });
  }
});

/*-----------------------------------------------------------------------
      __                    _         _         __  __                              _____   ____  
     / /                   | |       | |       |  \/  |                            |  __ \ |  _ \ 
    / /   _   _  _ __    __| |  __ _ | |_  ___ | \  / |  ___   _ __    __ _   ___  | |  | || |_) |
   / /   | | | || '_ \  / _` | / _` || __|/ _ \| |\/| | / _ \ | '_ \  / _` | / _ \ | |  | ||  _ < 
  / /    | |_| || |_) || (_| || (_| || |_|  __/| |  | || (_) || | | || (_| || (_) || |__| || |_) |
 /_/      \__,_|| .__/  \__,_| \__,_| \__|\___||_|  |_| \___/ |_| |_| \__, | \___/ |_____/ |____/ 
                | |                                                    __/ |                      
                |_|                                                   |___/                       
-----------------------------------------------------------------------*/

app.get("/updateMongoDB", async (req, res) => {
  console.log(req.query);

  let editedData = req.query.editedData ? JSON.parse(req.query.editedData) : undefined;
  let deletedData = req.query.deletedData;
  let collectionName = req.query.updateSection;

  // Delete data
  if (deletedData) {
    for (let x = 0; x < deletedData.length; x++) {
      deletedData[x] = deletedData[x].substring(1, deletedData[x].length - 1).replace(/"/g, '').split(",");
      await db.collection(collectionName).deleteOne({ _id: ObjectId(deletedData[x][deletedData[x].length - 1]) });
    }
    createConflicts();
    createEmails();
  }

  // Update edited data
  if (editedData) {
    let previousEntry = await db.collection(collectionName).findOne({ _id: ObjectId(editedData.id) });
    try {
      switch (collectionName) {
        case "athletes":
          await db.collection(collectionName).updateOne(
            { _id: ObjectId(editedData.id) },
            {
              $set:
              {
                "Student Email": editedData["Student Email"],
                Name: editedData.Name,
                Sport: editedData.Sport
              }
            });
          const rosterCursor = db.collection(collectionName).aggregate(rosterAggPipeline);
          await rosterCursor.toArray();
          break;
        case "events":
          await db.collection(collectionName).updateOne(
            { _id: ObjectId(editedData.id) },
            {
              $set:
              {
                Date: editedData.Date,
                Opponent: editedData.Opponent,
                Time: editedData.Time,
                Location: editedData.Location,
                "Departure Time": editedData["Departure Time"],
                Sport: editedData.Sport
              },
              $unset:
              {
                full_datetime: "",
                formatted_full_datetime: "",
                formatted_date: "",
                day: ""
              }
            });
          const eventCursor = db.collection(collectionName).aggregate(eventAggPipeline);
          await eventCursor.toArray();
          break;
        case "student-schedules":
          await db.collection(collectionName).updateOne(
            { _id: ObjectId(editedData.id) },
            {
              $set:
              {
                STUDENT_EMAIL: editedData.STUDENT_EMAIL,
                YR_CDE: editedData.YR_CDE,
                TRM_CDE: editedData.TRM_CDE,
                CRS_CDE: editedData.CRS_CDE,
                room_cde: editedData.room_cde,
                MONDAY_CDE: editedData.MONDAY_CDE == "" ? null : editedData.MONDAY_CDE,
                TUESDAY_CDE: editedData.TUESDAY_CDE == "" ? null : editedData.TUESDAY_CDE,
                WEDNESDAY_CDE: editedData.WEDNESDAY_CDE == "" ? null : editedData.WEDNESDAY_CDE,
                THURSDAY_CDE: editedData.THURSDAY_CDE == "" ? null : editedData.THURSDAY_CDE,
                FRIDAY_CDE: editedData.FRIDAY_CDE == "" ? null : editedData.FRIDAY_CDE,
                SATURDAY_CDE: editedData.SATURDAY_CDE == "" ? null : editedData.SATURDAY_CDE,
                BEGIN_TIM: editedData.BEGIN_TIM,
                END_TIM: editedData.END_TIM,
                BEGIN_DTE: editedData.BEGIN_DTE,
                END_DTE: editedData.END_DTE,
                Professor: editedData.Professor,
                PROFESSOR_EMAIL: editedData.PROFESSOR_EMAIL
              },
              $unset:
              {
                class_days: []
              }
            });
          const scheduleCursor = db.collection(collectionName).aggregate(studentSchedulesAggPipeline);
          await scheduleCursor.toArray();
          break;
        case "exam-schedule":
          let entry_file_is_from = await db.collection("files_uploaded").findOne({ file_name: previousEntry.file_name });
          let mondayOfFinals = entry_file_is_from.mondayOfFinals;
          await db.collection(collectionName).updateOne(
            { _id: ObjectId(editedData.id) },
            {
              $set:
              {
                COURSE: editedData.COURSE,
                INSTRUCTOR: editedData.INSTRUCTOR,
                EXAM_DAY: getFinalsDate(editedData.EXAM_DAY, mondayOfFinals),
                EXAM_TIME: editedData.EXAM_TIME,
                EXAM_ROOM: editedData.EXAM_ROOM,
              },
              $unset:
              {
                start_time: "",
                course_code: "",
                formatted_date: "",
                dayOfMonth: "",
                month: "",
                year: ""
              }
            });

          const finalCursor = db.collection("exam-schedule").aggregate(finalAggPipeline);
          await finalCursor.toArray();
          break;
      }
      const conflictsCursor = await db.collection("student-schedules").aggregate(conflictsAggPipeline);
      await conflictsCursor.toArray();
      const finalConflictCursor = db.collection("student-schedules").aggregate(finalConflictAggPipeline);
      await finalConflictCursor.toArray();
      createEmails();
    } catch (error) {
      switch (collectionName) {
        case "athletes":
          await db.collection(collectionName).updateOne(
            { _id: ObjectId(editedData.id) },
            {
              $set:
              {
                "Student Email": previousEntry["Student Email"],
                Name: previousEntry.Name,
                Sport: previousEntry.Sport
              }
            });

          res.send("An error was caught, preventing the system from crashing. Data entered in the table was deemed incorrect for the system. Check the backend logs for the error present. The entry that caused this was:\n\n" +
            "Student Email: " + editedData["Student Email"] +
            "\nName: " + editedData.Name +
            "\nSport: " + editedData.Sport
          );

          const eventCursor = db.collection(collectionName).aggregate(eventAggPipeline);
          await eventCursor.toArray();
          break;
        case "events":
          await db.collection(collectionName).updateOne(
            { _id: ObjectId(editedData.id) },
            {
              $set:
              {
                Date: previousEntry.Date,
                Opponent: previousEntry.Opponent,
                Time: previousEntry.Time,
                Location: previousEntry.Location,
                "Departure Time": previousEntry["Departure Time"],
                Sport: previousEntry.Sport
              },
              $unset:
              {
                full_datetime: "",
                formatted_full_datetime: "",
                formatted_date: "",
                day: ""
              }
            });

          res.send("An error was caught, preventing the system from crashing. Data entered in the table was deemed incorrect for the system. Check the backend logs for the error present. The entry that caused this was:\n\n" +
            "Date: " + editedData.Date +
            "\nOpponent: " + editedData.Opponent +
            "\nTime: " + editedData.Time +
            "\nLocation: " + editedData.Location +
            "\nDeparture Time: " + editedData["Departure Time"] +
            "\nSport: " + editedData.Sport
          );
          break;
        case "student-schedules":
          await db.collection(collectionName).updateOne(
            { _id: ObjectId(editedData.id) },
            {
              $set:
              {
                STUDENT_EMAIL: previousEntry.STUDENT_EMAIL,
                YR_CDE: previousEntry.YR_CDE,
                TRM_CDE: previousEntry.TRM_CDE,
                CRS_CDE: previousEntry.CRS_CDE,
                room_cde: previousEntry.room_cde,
                MONDAY_CDE: previousEntry.MONDAY_CDE,
                TUESDAY_CDE: previousEntry.TUESDAY_CDE,
                WEDNESDAY_CDE: previousEntry.WEDNESDAY_CDE,
                THURSDAY_CDE: previousEntry.THURSDAY_CDE,
                FRIDAY_CDE: previousEntry.FRIDAY_CDE,
                SATURDAY_CDE: previousEntry.SATURDAY_CDE,
                BEGIN_TIM: previousEntry.BEGIN_TIM,
                END_TIM: previousEntry.END_TIM,
                BEGIN_DTE: previousEntry.BEGIN_DTE,
                END_DTE: previousEntry.END_DTE,
                Professor: previousEntry.Professor,
                PROFESSOR_EMAIL: previousEntry.PROFESSOR_EMAIL
              },
              $unset:
              {
                class_days: []
              }
            });

          const scheduleCursor = db.collection(collectionName).aggregate(studentSchedulesAggPipeline);
          await scheduleCursor.toArray();

          res.send("An error was caught, preventing the system from crashing. Data entered in the table was deemed incorrect for the system. Check the backend logs for the error present. The entry that caused this was:\n\n" +
            "STUDENT_EMAIL: " + editedData.STUDENT_EMAIL +
            "\nYR_CDE: " + editedData.YR_CDE +
            "\nTRM_CDE: " + editedData.TRM_CDE +
            "\nCRS_CDE: " + editedData.CRS_CDE +
            "\nroom_cde: " + editedData.room_cde +
            "\nMONDAY_CDE: " + editedData.MONDAY_CDE +
            "\nTUESDAY_CDE: " + editedData.TUESDAY_CDE +
            "\nWEDNESDAY_CDE: " + editedData.WEDNESDAY_CDE +
            "\nTHURSDAY_CDE: " + editedData.THURSDAY_CDE +
            "\nFRIDAY_CDE: " + editedData.FRIDAY_CDE +
            "\nSATURDAY_CDE: " + editedData.SATURDAY_CDE +
            "\nBEGIN_TIM: " + editedData.BEGIN_TIM +
            "\nEND_TIM: " + editedData.END_TIM +
            "\nBEGIN_DTE: " + editedData.BEGIN_DTE +
            "\nEND_DTE: " + editedData.END_DTE +
            "\nProfessor: " + editedData.Professor +
            "\nPROFESSOR_EMAIL: " + editedData.PROFESSOR_EMAIL
          );
          break;
      }
      const conflictsCursor = await db.collection("student-schedules").aggregate(conflictsAggPipeline);
      await conflictsCursor.toArray();
      const finalConflictCursor = db.collection("student-schedules").aggregate(finalConflictAggPipeline);
      await finalConflictCursor.toArray();

      console.log("Error caught in the backend within the /updateMongoDB call:", error);
      return;
    }
    res.send("Complete");
    return;
  }
});

/*-----------------------------------------------------------------------
     __                             _   _       
    / /   ___   _ __ ___     __ _  (_) | |  ___ 
   / /   / _ \ | '_ ` _ \   / _` | | | | | / __|
  / /   |  __/ | | | | | | | (_| | | | | | \__ \
 /_/     \___| |_| |_| |_|  \__,_| |_| |_| |___/
                                                
-----------------------------------------------------------------------*/
app.get("/emails", async (req, res) => {
  try {
    console.log("[Node_Server] attempting to get emails_to_send")
    const aggCursor4 = db.collection(emailCollectionName).find({});
    const result = []

    for await (const doc of aggCursor4) {
      result.push(doc)
    }
    console.log("[Node_Server] got emails_to_send: ", result)
    res.send(result);
  } catch (error) {
    res.send({ status: 500, data: "Error gatherine emails" });
  }
});

app.get("/files_uploaded", async (req, res) => {
  console.log("[Node_Server] attempting to get files_uploaded")
  const aggCursor5 = db.collection(filesUploadedCollectionName).find({})
  const result = []

  for await (const doc of aggCursor5) {
    result.push(doc)
  }
  console.log("[Node_Server] got files_uploaded: ", result)
  res.send(result);
});

app.get("/changeEmailNotifications", async (req, res) => {
  var name = req.query.name;
  var date = req.query.date;
  var departure_time = req.query.departure_time;
  var course_code = req.query.course_code;
  var state = req.query.state;
  await updateConflictEmailNotifications(name, date, departure_time, course_code, state);
  res.send(`Document for ${name} updated`)
})

async function updateConflictEmailNotifications(name, date, departure_time, course_code, state) {
  let conflict = await db.collection("conflicts").findOneAndUpdate(
    {
      athlete_name: name,
      date_of_conflict: date,
      departure_time: departure_time,
      course: course_code
    },
    { $set: { disable_emails: state } }
  );
  await db.collection("emails_to_send").findOneAndUpdate(
    {
      roster_id: ObjectId(conflict.value.roster_id),
      class_id: ObjectId(conflict.value.class_id),
      event_id: ObjectId(conflict.value.event_id)
    },
    { $set: { disable_emails: state } }
  );
  await createEmails();
}

/*-----------------------------------------------------------------------
     __                  _                       _    __   _   _        
    / /  _   _   _ __   | |   ___     __ _    __| |  / _| (_) | |   ___ 
   / /  | | | | | '_ \  | |  / _ \   / _` |  / _` | | |_  | | | |  / _ \
  / /   | |_| | | |_) | | | | (_) | | (_| | | (_| | |  _| | | | | |  __/
 /_/     \__,_| | .__/  |_|  \___/   \__,_|  \__,_| |_|   |_| |_|  \___|
                |_|                                                             
-----------------------------------------------------------------------*/

app.get("/uploadfile", async (req, res) => {
  console.log("[Node_Server] req.url:", req.url);
  console.log("[Node_Server] req.params:", req.params);
  console.log("[Node_Server] req.query:", req.query);
  let file = req.query.file;

  //like a table in sql
  let collectionName;

  //create a variable that's an array
  //in each of these switch statements
  //set an array of values that are valid for that collection
  switch (req.query.selectedFileType) {
    case "Roster":
      collectionName = "athletes";
      break;
    case "Sport Schedule":
      collectionName = "events";
      break;
    case "Class Schedule":
      collectionName = "student-schedules";
      break;
    case "Final Exam Schedule":
      collectionName = "exam-schedule";
      updateFinalsDates(file, req.query.mondayOfFinals);
      break;
    default:
      console.log("[Node_Server] unknown file type!")
  }

  console.log("[Node_Server] DB: " + dbName);
  console.log("[Node_Server] FILENAME: " + file);
  console.log("[Node_Server] TYPE: " + typeof file);

  console.log("[Node_Server] COLLECTION: " + collectionName);
  console.log("[Node_Server] TYPE: " + typeof collectionName);

  // Runs a MongoCLI command that parses the CSV files into the database
  await exec(
    "mongoimport --uri " + url + " -d " +
    dbName +
    ' -c "' +
    collectionName +
    '" --type csv --file ../nuxt_website/csv/"' +
    file +
    '" --headerline --ignoreBlanks')
    .then(data => console.log(JSON.stringify(data)))
    .catch(err => {
      console.log("ERROR:\n" + err)
      res.json({ status: 500, data: "Error importing file" })
    });
  await createEmails();

  var ts = Date.now()
  var date = new Date(ts).toLocaleString("en-US", { timeZone: "America/Chicago" })
  var expiration = new Date(expirationDate(collectionName, ts)).toLocaleString("en-US", { timeZone: "America/Chicago" }).split(",")[0]

  // already_exists will be null if there is no file in the system with that name
  let already_exists = await db.collection(filesUploadedCollectionName).findOne({ file_name: file });
  if (already_exists != null) {
    // File already exists with that name, rename file
    let count = 1;           // Counter used to name files
    let foundName = false;   // Have we found a unique name to save the file as?
    while (!foundName) {
      let test_file_name = file.substring(0, file.indexOf('.')) + "(" + count + ")" + file.substring(file.indexOf('.'));
      already_exists = await db.collection(filesUploadedCollectionName).findOne({ file_name: test_file_name });
      if (already_exists != null) {
        count++;
      } else {
        file = test_file_name;
        foundName = true;
      }
    }
  }

  if (collectionName == "exam-schedule") {
    await db.collection(filesUploadedCollectionName).insertOne({
      file_name: file,
      date_added: date,
      expiration_date: expiration,
      mondayOfFinals: req.query.mondayOfFinals
    });
  } else {
    await db.collection(filesUploadedCollectionName).insertOne({
      file_name: file,
      date_added: date,
      expiration_date: expiration
    });
  }


  // if (process.env.DOCKER == 1) {
  //   await exec("cd /nuxt_website/csv/; mv " + file + " /csv/used")
  //     .then(data => console.log(JSON.stringify(data)))
  //     .catch(err => console.log("ERROR:\n" + err));
  // } else {
  //   await exec("cd /home/ad.msoe.edu/athcomm/nuxt_website/csv/; mv " + file + " /home/ad.msoe.edu/athcomm/csv/used")
  //     .then(data => console.log(JSON.stringify(data)))
  //     .catch(err => console.log("ERROR:\n" + err));
  // }

  // The following cursors are responsible for the conflict-identifying logic:
  await createConflicts();
  console.log("[Node_Server] updated collections and moved CSV file to 'used' folder")
  res.json({ status: 200, data: "successfully uploaded file and created conflicts" })

  // Assigns expiry dates to entries in the database
  // Adds name of file to each entry
  await db.collection(collectionName).updateMany(
    {
      expiration_date: { $exists: false },
      file_name: { $exists: false }
    },
    {
      $set:
      {
        expiration_date: expiration,
        file_name: file
      }
    }
  )
});

app.get("/deleteFile", async (req, res) => {
  let file_name = req.query.file;
  let sent_emails_cursor = await db.collection(emailCollectionName).find({ sent: true });
  let sent_emails = [];
  let sent = false;

  for await (const email of sent_emails_cursor) {
    sent_emails.push(email);
  }

  for (let x = 0; x < sent_emails.length; x++) {
    let athlete = await db.collection(athletesCollectionName).findOne({ _id: ObjectId(sent_emails[x].roster_id) });
    let schedule = await db.collection(schedulesCollectionName).findOne({ _id: ObjectId(sent_emails[x].class_id) });
    let event = await db.collection(eventCollectionName).findOne({ _id: ObjectId(sent_emails[x].event_id) });

    if (athlete.file_name == file_name || schedule.file_name == file_name || event.file_name == file_name) {
      // File has already caused an email to be sent
      sent = true;
    }
  }

  if (sent) {
    res.send("Email already sent");
  } else {
    // File has not yet caused an email to be sent. Go ahead with deletion.
    await db.collection(eventCollectionName).deleteMany({ file_name: file_name });
    await db.collection(athletesCollectionName).deleteMany({ file_name: file_name });
    await db.collection(schedulesCollectionName).deleteMany({ file_name: file_name });
    await db.collection(finalExamCollectionName).deleteMany({ file_name: file_name });

    await db.collection(filesUploadedCollectionName).deleteOne({ file_name: file_name });

    // Need to update conflicts
    const conflictsCursor = db.collection("student-schedules").aggregate(conflictsAggPipeline);
    await conflictsCursor.toArray();
    const finalConflictCursor = db.collection("student-schedules").aggregate(finalConflictAggPipeline);
    await finalConflictCursor.toArray();

    // Update emails
    await createConflicts();

    res.send("Complete");
  }
});

const days_to_seconds = 86400 * 1000; // number of seconds in a day
function expirationDate(filetype, time) {
  const days = (filetype === "student-schedules" ? ["1-1", "6-1"] : ["6-1"]);
  var year = new Date().getFullYear() - 1;
  var expiration = 0;
  for (var attempts = 0; attempts < 10; attempts++) { // limiting how long it can run to prevent infinite loops
    for (let i = 0; i < days.length; i++) {
      expiration = new Date(`${year}-${days[i]}`);
      if (time + days_to_seconds * 7 < expiration.getTime()) return expiration;
      /* 
      Note: The time is adjusted 7 days into the future, 
      so that if you upload files towards the end of a term,
      it doesn't set the expiry to be the (very very close) end of the term.
      Might save some headaches
      */
    }
    year++;
  }
  return expiration;
}

/*-----------------------------------------------------------------------
     __  _                  _                                _   _ 
    / / | |_    ___   ___  | |_    ___   _ __ ___     __ _  (_) | |
   / /  | __|  / _ \ / __| | __|  / _ \ | '_ ` _ \   / _` | | | | |
  / /   | |_  |  __/ \__ \ | |_  |  __/ | | | | | | | (_| | | | | |
 /_/     \__|  \___| |___/  \__|  \___| |_| |_| |_|  \__,_| |_| |_|
                                                                           
-----------------------------------------------------------------------*/
app.get("/testemail", async (req, res) => {
  console.log("Email sending tests starting...");
  try {
    testing = true;
    sendEmails(false, false);
  } catch (err) {
    console.log("ERROR!");
  };
  res.json({
    status: 200,
    data: "Finished email sending test."
  })
});

/*-----------------------------------------------------------------------
     __      _          _          _            ____                                                      _         
    / /   __| |   ___  | |   ___  | |_    ___  |  _ \    ___     ___   _   _   _ __ ___     ___   _ __   | |_   ___ 
   / /   / _` |  / _ \ | |  / _ \ | __|  / _ \ | | | |  / _ \   / __| | | | | | '_ ` _ \   / _ \ | '_ \  | __| / __|
  / /   | (_| | |  __/ | | |  __/ | |_  |  __/ | |_| | | (_) | | (__  | |_| | | | | | | | |  __/ | | | | | |_  \__ \
 /_/     \__,_|  \___| |_|  \___|  \__|  \___| |____/   \___/   \___|  \__,_| |_| |_| |_|  \___| |_| |_|  \__| |___/
                                                                                                                    
-----------------------------------------------------------------------*/
// Deleting entries
app.get("/deleteDocuments", async (req, res) => {
  db.collection("events").deleteMany({});
  db.collection("conflicts").deleteMany({});
  db.collection("student-schedules").deleteMany({});
  db.collection("emails_to_send").deleteMany({});
  db.collection("athletes").deleteMany({});
  db.collection("exam-schedule").deleteMany({});
  db.collection("files_uploaded").deleteMany({});
  res.send("Database emptied.")
});


async function putConflict(
  id,
  professor_email,
  conflict_description,
  student_id,
  conflict_date
) {
  try {
    const { data } = await axios({
      method: "put",
      url: "https://rsp2d2rxh7.execute-api.us-east-1.amazonaws.com/emailtosend",
      data: {
        id: id,
        professor_email: professor_email,
        conflict_description: conflict_description,
        student_id: student_id,
        conflict_date: conflict_date,
      },
    });
    console.log(data);
  } catch (err) {
    console.log(err);
  }
}
/*----------------------------------------------------------------------
                          _  _    __                      _    _                    
                         (_)| |  / _|                    | |  (_)                   
   ___  _ __ ___    __ _  _ | | | |_  _   _  _ __    ___ | |_  _   ___   _ __   ___ 
  / _ \| '_ ` _ \  / _` || || | |  _|| | | || '_ \  / __|| __|| | / _ \ | '_ \ / __|
 |  __/| | | | | || (_| || || | | |  | |_| || | | || (__ | |_ | || (_) || | | |\__ \
  \___||_| |_| |_| \__,_||_||_| |_|   \__,_||_| |_| \___| \__||_| \___/ |_| |_||___/

----------------------------------------------------------------------*/
// cron
const cron = require('node-cron');
const e = require("express");
const { CLIENT_RENEG_WINDOW } = require("tls");

/**
 * Schedules emails to be sent weekly.
 *
 * cron uses UTC, so noon on Friday is '0 18 * * FRI'.
 * Since this is NOT DST aware, it will run at 1pm during Daylight Savings.
 */
var task = cron.schedule('00 18 * * FRI', () => {
  //  cron.schedule('* * * * *', () => {
  console.log("[Node_Server " + (new Date()).toString().replace(/\+0000 \(.*\)/, "") + "] Sending Emails...");
  if (!testing) {
    sendEmails(true, false);
  }
});


// constants
const bcc = "28ced8b6.msoe365.onmicrosoft.com@amer.teams.ms"; // Teams channel that will get the emails. Good for verifying the emails are A. sending and B. look as expected
const weeklySubject = "Next week's athletic conflicts";
const lateInitialSubject = "New conflicts notice for this week"
const updateSubject = "UPDATE to athletic conflict";
const removedSubject = "Athletic conflict no longer exists";
const openingLine = "You are being contacted because one or more of your students is a student athlete and may miss class for an upcoming athletic competition.";
const lateInitialOpeningLine = "You are being contacted because a change/addition to a sports schedule has created a conflict for one or more of the students in your class. The following students may miss class for an upcoming athletic competition."
const changingLine = "You are being contacted because you previously recieved an email about an athlete missing your class for an upcoming athletic competition.";
const newProfessor = "You are being contacted because a recent change in the athletic communicator website has resulted in your name being moved to a new class.\nOne of your students is a student athlete and may miss class for an upcoming athletic competition:";
const previousMessage = "The previous email you received is as follows:"
const removedConflict = "This conflict no longer exists.\n\nThe previous conflict was as follows:";
const updateConflict = "This conflict has been UPDATED to the following:";
const closingLine = "Please feel free to reach out to the Athletic Director, Brian Miller (miller@msoe.edu), Faculty Athletic Representative, Anne-Marie Nickel (nickel@msoe.edu), or Compliance Director, Bill Massoels (massoels@msoe.edu) if you have any questions or concerns. Thank you so much for your time and patience when working with these student-athletes.";
let testing;

if (process.env.TESTING_EMAIL) {
  testing = Number(process.env.TESTING_EMAIL);
} else {
  // Set to false when actually doing emails
  testing = true;
}

console.log("[Node_Server] testing is set to: ", testing)
if (!testing) {
  console.log("[Node_Server] Testing is at a value that will allow emails to be sent.");
}
let testEmail;
if (process.env.TEST_EMAIL) {
  testEmail = process.env.TEST_EMAIL
} else {
  testEmail = "23b04c36.msoe365.onmicrosoft.com@amer.teams.ms"
}
console.log("[Node_Server] testing email is set to: ", testEmail)
emailCollectionName = "emails_to_send";
conflictCollectionName = "conflicts";
eventCollectionName = "events";
athletesCollectionName = "athletes";
schedulesCollectionName = "student-schedules";
finalExamCollectionName = "exam-schedule"
filesUploadedCollectionName = "files_uploaded";

/**
 * Create Emails from the conflict collection
 * @returns 0
 */
async function createEmails(db) {
  if (!db) {
    db = client.db("athcomm");
  }

  const items = await db.collection(conflictCollectionName).find({ sports_match: true }).toArray();
  try {
    for (let i = 0; i < items.length; i++) {
      var conflict = items[i];
      let conflictDescription = conflict["athlete_name"] + " has a departure time of " + conflict["departure_time"] + " for a sports competition and may miss " + conflict["course"].replace(/\s+/g, ' ').trim() + " on " + conflict["date_of_conflict"] + ".";
      let matchingEmail = await db.collection(emailCollectionName).findOne({
        roster_id: ObjectId(conflict["roster_id"]),
        class_id: ObjectId(conflict["class_id"]),
        event_id: ObjectId(conflict["event_id"])
      });
      if (matchingEmail == null) {
        // Email has not been created yet. Create new email
        await db.collection(emailCollectionName).insertOne({
          conflict_date: conflict["date_of_conflict"],
          professor_email: conflict["professor_email"],
          professor_name: conflict["professor"],
          conflict_description: conflictDescription,
          sent: false,
          roster_id: ObjectId(conflict["roster_id"]),
          class_id: ObjectId(conflict["class_id"]),
          event_id: ObjectId(conflict["event_id"]),
          disable_emails: "false"
        });
      } else {
        // Email already exists. Check for changes.
        if (matchingEmail["professor_name"] != conflict["professor"] ||
          matchingEmail["professor_email"] != conflict["professor_email"] ||
          matchingEmail["conflict_description"] != conflictDescription ||
          (matchingEmail["disable_emails"] != conflict["disable_emails"] &&
            conflict["disable_emails"] != null)) {
          // Change has occured
          if (matchingEmail["sent"] == true) {
            console.log("Change to previously sent email detacted. Sending follow up email");
            // Follow up email needed
            // Update the email
            await db.collection(emailCollectionName).updateOne(
              {
                roster_id: ObjectId(conflict["roster_id"]),
                class_id: ObjectId(conflict["class_id"]),
                event_id: ObjectId(conflict["event_id"])
              },
              {
                $set:
                {
                  conflict_date: conflict["date_of_conflict"],
                  professor_email: conflict["professor_email"],
                  professor_name: conflict["professor"],
                  conflict_description: conflictDescription,
                  disable_emails: conflict["disable_emails"]
                }
              }
            );

            // Send the follow up email
            let content = "Bcc: " + bcc + "\n";
            content += "Subject: " + updateSubject + "\n\n";
            console.log("matchingEmail:", matchingEmail);
            console.log("conflict:", conflict);
            if (matchingEmail["professor_name"] != conflict["professor"] &&
              matchingEmail["professor_email"] != conflict["professor_email"] &&
              matchingEmail["conflict_description"] == conflictDescription) {
              // A new professor has been assigned to a class and never recieved an initial email.
              content += "Dear Professor " + conflict["professor"].split(',')[0] + ",\n\n" + newProfessor + "\n\n";
              content += matchingEmail["conflict_description"];
            } else {
              // The same professor is on the class and needs the basic follow up email.
              content += "Dear Professor " + matchingEmail["professor_name"].split(',')[0] + ",\n\n" + changingLine + "\n\n" + previousMessage + "\n";
              content += matchingEmail["conflict_description"] + "\n\n" + updateConflict + "\n";
              content += conflictDescription + "\n";
            }

            // Send email 
            await sendEmail(content, true, matchingEmail["conflict_description"], matchingEmail["professor_email"], db);

            // Re-mark the conflict as sent so it does not show up on the conflicts page.
            await db.collection(conflictCollectionName).updateOne(
              {
                roster_id: ObjectId(matchingEmail["roster_id"]),
                class_id: ObjectId(matchingEmail["class_id"]),
                event_id: ObjectId(matchingEmail["event_id"])
              },
              {
                $set:
                {
                  sent: true
                }
              }
            );
          } else {
            // Email has not been sent yet, update the email object
            await db.collection(emailCollectionName).updateOne(
              {
                roster_id: ObjectId(conflict["roster_id"]),
                class_id: ObjectId(conflict["class_id"]),
                event_id: ObjectId(conflict["event_id"])
              },
              {
                $set:
                {
                  conflict_date: conflict["date_of_conflict"],
                  professor_email: conflict["professor_email"],
                  professor_name: conflict["professor"],
                  conflict_description: conflictDescription,
                  disable_emails: conflict["disable_emails"]
                }
              }
            );
          }
        } else if (matchingEmail["sent"] == true) {
          // Email has already been sent and no change has been detected.
          // Re-mark the conflict as sent so it does not show up on the conflicts page.
          await db.collection(conflictCollectionName).updateOne(
            {
              roster_id: ObjectId(matchingEmail["roster_id"]),
              class_id: ObjectId(matchingEmail["class_id"]),
              event_id: ObjectId(matchingEmail["event_id"])
            },
            {
              $set:
              {
                sent: true
              }
            }
          );
        }
      }
    }

    const emails = await db.collection(emailCollectionName).find().toArray();
    for (let i = 0; i < emails.length; i++) {
      let email = await emails[i];
      let matchingConflict = await db.collection(conflictCollectionName).findOne({
        roster_id: ObjectId(email["roster_id"]),
        class_id: ObjectId(email["class_id"]),
        event_id: ObjectId(email["event_id"])
      });
      if (matchingConflict == null) {
        // No conflict exists for this email anymore
        if (email["sent"] == true) {
          // Email has been sent. Need a follow up email
          // Send the follow up email
          let content = "Bcc: " + bcc + "\n";
          content = "Subject: " + removedSubject + "\n\n";
          content += "Dear Professor " + email["professor_name"].split(',')[0] + ",\n\n" + changingLine + "\n" + removedConflict + "\n";
          content += email["conflict_description"]
          await sendEmail(content, true, email["conflict_description"], email["professor_email"], db);

          // Delete the email that no longer exists
          await db.collection(emailCollectionName).deleteOne({ _id: ObjectId(email["_id"]) })
        } else {
          // Email has not been sent. Delete email
          await db.collection(emailCollectionName).deleteOne({ _id: ObjectId(email["_id"]) })
        }
      } else {
        if (email.disable_emails === "true") {
          // Reset conflict object's disabled_state to be true
          await db.collection(conflictCollectionName).findOneAndUpdate(
            {
              _id: ObjectId(matchingConflict._id)
            },
            { $set: { disable_emails: "true" } });
        }
      }
    }
    sendEmails(true, true);
  } catch (err) {
    console.log(err);
  }
  return 0;
}

function execPromise(command) {
  return new Promise(function (resolve, reject) {
    exec(command, (error, stdout, stderr) => {
      if (error) {
        reject(error);
        return;
      }
      resolve(stdout.trim());
    });
  });
}

/**
 * Sends the emails in the "emails_to_send" collection.
 * Emails with matching professors will have their conflict descriptions combined into one message so the professor doesn't get spammed.
 * @param {* if true, sends the emails to the professor, otherwise if sends to the Teams Channel} sendToProf 
 * If testing is hardcoded as true, the emails don't get sent to the professors.
 * @returns void
 */
async function sendEmails(sendToProf, sendInitial, db, runningTestCases) {
  if (!db) {
    db = client.db("athcomm");
  }
  if (sendToProf == "true") {
    sendToProf = true;
  } else {
    sendToProf = false;
  }
  if (testing) {
    sendToProf = false;
  }

  let today = new Date();

  // The two time bounadaries to send emails between
  let nextMondayDate = new Date(new Date(new Date(today).setDate(today.getDate() + ((7 - today.getDay()) % 7 + 1))).setHours(0, 0, 0, 0));
  let followingMondayDate = new Date(new Date(new Date(today).setDate(nextMondayDate.getDate() + 7)).setHours(0, 0, 0, 0));
  let nearFutureConflicts = [];
  await client.connect();
  const items = await db.collection(emailCollectionName).find({}).toArray();
  try {
    if (sendInitial) {
      for (let i = 0; i < items.length; i++) {
        if (items[i]["disable_emails"] != "true" && items[i]["sent"] != true && lateInitialEmailRequired(items[i]["conflict_date"])) {
          nearFutureConflicts.push(items[i]);
          if (!sendToProf) {
            nearFutureConflicts[nearFutureConflicts.length - 1]["professor_email"] = testEmail;
          }
        }
      }
    } else {
      for (let i = 0; i < items.length; i++) {
        let date = new Date(items[i]["conflict_date"]);
        if ((date < followingMondayDate) && (date >= nextMondayDate) && items[i]["disable_emails"] != "true" && items[i]["sent"] != true) {
          nearFutureConflicts.push(items[i]);
          if (!sendToProf) {
            nearFutureConflicts[nearFutureConflicts.length - 1]["professor_email"] = testEmail;
          }
        }
      }
    }

  }
  catch (err) {
    console.log(err);
  }

  // iterates through nearFutureConflicts and iterates through it again
  // for every element in nearFutureConflicts, if there is another element with a matching professor email, the program will
  // combine the messages
  // delete the second element of the pair from the array
  // send an email
  for (let i = 0; i < nearFutureConflicts.length; i = i) {
    let content = "Bcc: " + bcc + "\n";
    if (sendInitial) {
      content += "Subject: " + lateInitialSubject + "\n\n";
      content += "Dear Professor " + nearFutureConflicts[i]["professor_name"].split(',')[0] + ",\n\n" + lateInitialOpeningLine + "\n\n";
    } else {
      content += "Subject: " + weeklySubject + "\n\n";
      content += "Dear Professor " + nearFutureConflicts[i]["professor_name"].split(',')[0] + ",\n\n" + openingLine + "\n\n";
    }
    content += nearFutureConflicts[i]["conflict_description"] + "\n";
    // iterates through the near future conflicts
    for (let j = i + 1; j < nearFutureConflicts.length; j++) {
      // if the object has the same professor email, the conflict descriptions will be combined
      if (nearFutureConflicts[i]["professor_name"] == nearFutureConflicts[j]["professor_name"]) {
        content += nearFutureConflicts[j]["conflict_description"] + "\n";
        // This line updates many since there could be a scenario where lab and lecture are on the same day, causing two conflict descriptions to be the same.
        db.collection(emailCollectionName).updateMany(
          {
            conflict_description: nearFutureConflicts[j]["conflict_description"]
          },
          {
            $set:
            {
              sent: true
            }
          }
        );
        // Need to update the second conflict to be marked as sent
        await db.collection(conflictCollectionName).updateOne(
          {
            roster_id: ObjectId(nearFutureConflicts[j]["roster_id"]),
            class_id: ObjectId(nearFutureConflicts[j]["class_id"]),
            event_id: ObjectId(nearFutureConflicts[j]["event_id"])
          },
          {
            $set:
            {
              sent: true
            }
          }
        );
        nearFutureConflicts.splice(j, 1);
        j--;
      }
    }
    content += "\n" + closingLine;

    await sendEmail(content, sendToProf, nearFutureConflicts[i]["conflict_description"], nearFutureConflicts[i]["professor_email"], db, runningTestCases);
    await db.collection(conflictCollectionName).updateOne(
      {
        roster_id: ObjectId(nearFutureConflicts[i]["roster_id"]),
        class_id: ObjectId(nearFutureConflicts[i]["class_id"]),
        event_id: ObjectId(nearFutureConflicts[i]["event_id"])
      },
      {
        $set:
        {
          sent: true
        }
      }
    );
    nearFutureConflicts.splice(i, 1);
  }
  return;
}

async function sendEmail(content, sendToProf, conflictDescription, profEmail, db, runningTestCases) {
  // generates email txt file
  fs.writeFileSync('newfile.txt', content, function (err) {
    if (err)
      throw err;
  });
  console.log(fs.readFileSync("newfile.txt", "utf8"));

  // the command that uses the generated file to send an email
  try {
    if (!sendToProf && !runningTestCases) {
      console.log('[Node_Server] msmtp test send.')
      await execPromise("cat newfile.txt | msmtp --file=/etc/msmtp/msmtprc -d " + "dethloffe@msoe.edu");
    } else if (!runningTestCases) {
      console.log('[Node_Server] msmtp real send.')
      // await execPromise("cat newfile.txt | msmtp --file=/etc/msmtp/msmtprc -d " + profEmail);
      await execPromise("cat newfile.txt | msmtp --file=/etc/msmtp/msmtprc -d " + "dethloffe@msoe.edu");
    }

    await db.collection(emailCollectionName).updateOne({ conflict_description: conflictDescription },
      {
        $set:
        {
          sent: true
        }
      }
    );
  } catch (e) {
    console.error(e.message);
  }
  return;
}

async function createConflicts(db) {
  if (!db) {
    db = client.db("athcomm");
  }

  console.log("[Node_Server] Beginning to create conflicts");
  // The following cursors are responsible for the conflict-identifying logic:
  // Adjusts fields in the events database required for determining conflicts
  const eventCursor = db.collection("events").aggregate(eventAggPipeline);
  await eventCursor.toArray();

  // Adjusts fields in the exam-schedule database required for determining final-exam conflicts
  const finalCursor = db.collection("exam-schedule").aggregate(finalAggPipeline);
  await finalCursor.toArray();

  // Adjusts fields in the student schedules database required for determining conflicts
  const studentScheduleCursor = db.collection("student-schedules").aggregate(studentSchedulesAggPipeline);
  await studentScheduleCursor.toArray();

  // Adjusts fields in the athletes database required for determining conflicts
  const rosterCursor = db.collection("athletes").aggregate(rosterAggPipeline);
  await rosterCursor.toArray();

  // Generates the conflict database
  const conflictsCursor = db.collection("student-schedules").aggregate(conflictsAggPipeline);
  await conflictsCursor.toArray();

  // Generates final exam conflicts
  const finalConflictCursor = db.collection("student-schedules").aggregate(finalConflictAggPipeline);
  await finalConflictCursor.toArray();

  createEmails();
  console.log("[Node_Server] updated conflicts");

  return "Complete";
}

async function updateFinalsDates(file, mondayDate) {
  let path = "../nuxt_website/csv/" + file;
  fs.readFile(path, 'utf8', function (err, data) {
    if (err) {
      console.log("ERROR READING FINAL SCHEDULE: ", err);
    } else {
      var result = data.replaceAll(/\b(?:Monday|Tuesday|Wednesday|Thursday|Friday)\b/gi, day => getFinalsDate(day, mondayDate));

      fs.writeFile(path, result, 'utf8', function (err) {
        if (err) return console.log("ERROR WRITING TO FINAL SCHEDULE: ", err);
      });
    }
  });
}

function getFinalsDate(weekDay, mondayDate) {
  let date = new Date(mondayDate);
  switch (weekDay) {
    case "Monday":
      return mondayDate;
    case "Tuesday":
      date.setDate(date.getDate() + 1);
      break;
    case "Wednesday":
      date.setDate(date.getDate() + 2);
      break;
    case "Thursday":
      date.setDate(date.getDate() + 3);
      break;
    case "Friday":
      date.setDate(date.getDate() + 4);
      break;
    case "Saturday":
      date.setDate(date.getDate() + 5);
      break;
  }
  return (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
}

function lateInitialEmailRequired(conflictDate, today) {
  if (today) {
    today = new Date(today);
  } else {
    today = new Date();
  }
  let nextMondayDate = new Date(new Date(new Date(today).setDate(today.getDate() + ((7 - today.getDay()) % 7 + 1))).setHours(0, 0, 0, 0));
  let followingMondayDate = new Date(new Date(new Date(today).setDate(nextMondayDate.getDate() + 7)).setHours(0, 0, 0, 0));
  conflictDate = new Date(conflictDate);

  return (
    (
      // Is today between friday at noon and sunday at midnight?
      ((today.getDay() == 5 && today.getHours() >= 17) || today.getDay() == 6 || today.getDay() == 0)
      &&
      // Is the unsent email conflict date between now and 2 mondays in the future?
      ((conflictDate < followingMondayDate) && (conflictDate > today || sameDay(conflictDate, today)))
    ) ||
    (
      // Today is between Monday 12:01 AM and Friday at noon
      // Is the unsent email conflict date between now and next Monday at 12:00AM?
      conflictDate < nextMondayDate && (conflictDate > today || sameDay(conflictDate, today))
    ));
}

function sameDay(day1, day2) {
  return day1.getDate() == day2.getDate() && day1.getMonth() == day2.getMonth() && day1.getFullYear() == day2.getFullYear();
}

/*-------

AUTHENTICATION (whoever does the block lettering thing can replace this)

*/

async function hash(input, salt, len) {
  return new Promise(function (resolve, reject) {
    crypto.scrypt(input, salt, len, (err, hash) => {
      return err ? reject(err) : resolve(hash.toString('hex'));
    });
  })
}

const salt = 'spongebob'

app.get('/getHash', async (req, res) => {
  hash(req.query.password, salt, 24)
    .then(data => {
      let keys = db.collection("keys");
      keys.insertOne({
        hash: data,
        createdBy: new Date()
      });
      console.log('doc inserted')
      return data
    })
    .then(data => {
      res.send(data);
    })
    .catch(error => { console.log(error) })
})

app.get('/verifyHash', async (req, res) => {
  let token = req.query.token
  console.log(token)
  let cursor = await db.collection("keys").find({
    hash: token
  }).toArray()
  res.send(cursor.length > 0)
})
