// This MongoDB aggregation pipeline is responsible for generating the conflicts with final exams. It is run on the exam-schedule collection.
module.exports = [
  {
    $set: {
      EXAM_TIME: {
        $cond: [
          { $regexMatch: { input: "$EXAM_TIME", regex: /(.*)(\d+)(.*)/ } },
          "$EXAM_TIME",
          null
        ]
      },
    },
  },
  {
    $addFields: {
      start_time: { $substr: ["$EXAM_TIME", 0, 8] }
    },
  },
  {
    $addFields: {
      datetime: { $concat: ["$EXAM_DAY", " ", "$start_time"] },
    },
  },
  {
    $addFields: {
      course_code: { $replaceAll: { input: "$COURSE", find:" ", replacement: "" } },
      formatted_date: { $dateFromString: { dateString: "$datetime", onError: null } }
    }
  },
  {
    $addFields: {
      dayOfMonth: {$dayOfMonth: "$formatted_date" },
      month: {$month: "$formatted_date" },
      year: {$year: "$formatted_date" },
    }
  },
  { $out: "exam-schedule" },
]