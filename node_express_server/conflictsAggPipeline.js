// This MongoDB aggregation pipeline is responsible for generating the conflicts. It is run on the student-schedules collection.
module.exports = [
  {
    $lookup: { // Adds an athlete to an instance of a class if the roster email matches the email on the class on the registrar file.
      from: "athletes",
      localField: "STUDENT_EMAIL",
      foreignField: "Student Email",
      as: "athlete",
    },
  },
  { $unwind: { path: "$athlete" } },
  {
    $lookup: { // If an event occurs on the same day (Mon/Tues/etc.) as a course, it is added as an event on that day.
      from: "events",
      localField: "class_days",
      foreignField: "day",
      as: "events_on_this_day",
    },
  },
  { $unwind: { path: "$events_on_this_day" } },
  {
    $addFields: {
      athlete_sport: { $trim: { input: "$athlete.Sport" } },
      conflict_sport: { $trim: { input: "$events_on_this_day.Sport" } },
      professor_email: { $trim: { input: "$student-schedules.PROFESSOR_EMAIL" } },
      date_time: { input: "$events.formatted_full_datetime" }
    },
  },
  {
    $addFields: {
      sports_match: { $eq: ["$athlete_sport", "$conflict_sport"] }
    },
  },
  { // The following blocks of code add the time formatted in a way that it can be compared across documents
    // vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
    $addFields: {
      Departure_Time: '$events_on_this_day.Departure Time',
      formatted_depart: '$events_on_this_day.formatted_full_datetime',
    },
  },
  {
    $addFields: {
      full_class_time: {
        $concat: ["$events_on_this_day.Date", " ", "$END_TIM"]
      }
    }
  },
  {
    $addFields: {
      class_datetime: {
        $dateFromString: {
          dateString: "$full_class_time",
          onError: null
        },
      },
      class_end_date: {
        $dateFromString: {
          dateString: "$END_DTE",
          onError: null
        },
      },
      class_start_date: {
        $dateFromString: {
          dateString: "$BEGIN_DTE",
          onError: null
        },
      }
    },
  },
  {
    $addFields: {
      departure_with_variance: {
        $dateSubtract: {
          startDate: "$formatted_depart",
          unit: "hour",
          amount: 1, // Allows one hour of tolerance. Ex: if a class ends at 2:50 and departure time is 3:00, without tolerance this would not be considered a conflict, but realistically it should be. Hence the tolerance.
        }
      }
    }
  }, // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  {
    $addFields: {
      overlaps_with_class: {
        $and: [
          { $lte: ["$departure_with_variance", "$class_datetime"] }, // Compares the departure time with class time to determine if there is a conflict
        ]
      },
      after_end_date: {
        $lte: ["$class_end_date", "$events_on_this_day.formatted_date"]
      },
      before_start_date: {
        $gte: ["$class_start_date", "$events_on_this_day.formatted_date"]
      }
    }
  },
  {
    $project: {
      _id: 0,
      athlete_name: "$athlete.Name",
      athlete_sport: "$athlete.Sport",
      professor: "$Professor",
      course: "$CRS_CDE",
      course_begin_time: "$BEGIN_TIM",
      course_end_time: "$END_TIM",
      date_of_conflict: "$events_on_this_day.Date",
      departure_time: "$Departure_Time",
      professor_email: "$PROFESSOR_EMAIL",
      sports_match: "$sports_match",
      overlaps_with_class: "$overlaps_with_class",
      after_end_date: "$after_end_date",
      before_start_date: "$before_start_date",
      roster_id: "$athlete._id",
      class_id: "$_id",
      event_id: "$events_on_this_day._id"
    },
  },
  {
    $match: {
      sports_match: true,
      departure_time: { $ne: null, }, // Assumes that if there is on departure time there will be no conflicts
      overlaps_with_class: true,
      after_end_date: false,
      before_start_date: false
    },
  },
  { $out: "conflicts" },
]
