// This MongoDB aggregation pipeline is responsible for generating the conflicts with final exams. It is run on the exam-schedule collection.
module.exports = [
    {
      '$lookup': {
        'from': 'athletes', 
        'localField': 'STUDENT_EMAIL', 
        'foreignField': 'Student Email', 
        'as': 'athlete'
      }
    }, {
      '$unwind': {
        'path': '$athlete'
      }
    }, {
      '$lookup': {
        'from': 'exam-schedule', 
        'localField': 'c_code', 
        'foreignField': 'course_code', 
        'as': 'final'
      }
    }, {
      '$unwind': {
        'path': '$final'
      }
    }, {
      '$lookup': {
        'from': 'events', 
        'localField': 'final.dayOfMonth', 
        'foreignField': 'dayOfMonth', 
        'as': 'event'
      }
    }, {
      '$unwind': {
        'path': '$event'
      }
    }, {
      '$addFields': {
        'sports_match': {
          '$and': [
            {
              '$eq': [
                '$athlete_sport', '$conflict_sport'
              ]
            }, {
              '$eq': [
                '$event.dayOfMonth', '$final.dayOfMonth'
              ]
            }, {
              '$eq': [
                '$event.month', '$final.month'
              ]
            }, {
              '$eq': [
                '$event.year', '$final.year'
              ]
            }
          ]
        }, 
        'overlaps_with_class': {
          '$lte': [
            '$event.formatted_date', '$final.formatted_date'
          ]
        }
      }
    }, {
      '$project': {
        '_id': 0, 
        'athlete_name': '$athlete.Name', 
        'athlete_sport': '$athlete.Sport', 
        'professor': '$Professor', 
        'course': '$CRS_CDE', 
        'course_begin_time': '$final.start_time', 
        'course_end_time': '$final.EXAM_TIME', 
        'date_of_conflict': '$final.EXAM_DAY', 
        'departure_time': '$event.Departure Time', 
        'professor_email': '$PROFESSOR_EMAIL', 
        'sports_match': '$sports_match', 
        'overlaps_with_class': '$overlaps_with_class', 
        'roster_id': '$athlete._id', 
        'class_id': '$_id', 
        'event_id': '$event._id'
      }
    }, {
      '$match': {
        'sports_match': true, 
        'departure_time': {
          '$ne': null
        }, 
        'overlaps_with_class': true
      }
    }, {
      '$merge': {
        'into': 'conflicts', 
        'on': '_id', 
        'whenMatched': 'replace', 
        'whenNotMatched': 'insert'
      }
    }
]