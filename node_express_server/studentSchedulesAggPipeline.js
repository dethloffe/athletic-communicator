// This is run against the student schedules collection and adds a numerical value for each day of the week (except Sunday), used to compare days of the week across collections.
// MON = 2
// TUE = 3
// ...
// SAT = 7
module.exports = [
  { $addFields: { class_days: [] } },
  {
    $set: {
      BEGIN_TIM: {
        $cond: [
          { $regexMatch: { input: "$BEGIN_TIM", regex: /(.*)(\d+)(.*)/ } },
          "$BEGIN_TIM",
          null
        ]
      },
    },
  },
  {
    $set: {
      END_TIM: {
        $cond: [
          { $regexMatch: { input: "$END_TIM", regex: /(.*)(\d+)(.*)/ } },
          "$END_TIM",
          null
        ]
      },
    },
  },
  {
    $set: {
      BEGIN_DTE: {
        $cond: [
          { $regexMatch: { input: "$BEGIN_DTE", regex: /(.*)(\d+)(.*)/ } },
          "$BEGIN_DTE",
          null
        ]
      },
    },
  },
  {
    $set: {
      END_DTE: {
        $cond: [
          { $regexMatch: { input: "$END_DTE", regex: /(.*)(\d+)(.*)/ } },
          "$END_DTE",
          null
        ]
      },
    },
  },
  {
    $set: {
      class_days: {
        $cond: [
          { $eq: ["$MONDAY_CDE", "M"] },
          { $concatArrays: ["$class_days", [2]] },
          "$class_days",
        ],
      },
    },
  },
  {
    $set: {
      class_days: {
        $cond: [
          { $eq: ["$TUESDAY_CDE", "T"] },
          { $concatArrays: ["$class_days", [3]] },
          "$class_days",
        ],
      },
    },
  },
  {
    $set: {
      class_days: {
        $cond: [
          { $eq: ["$WEDNESDAY_CDE", "W"] },
          { $concatArrays: ["$class_days", [4]] },
          "$class_days",
        ],
      },
    },
  },
  {
    $set: {
      class_days: {
        $cond: [
          { $eq: ["$THURSDAY_CDE", "R"] },
          { $concatArrays: ["$class_days", [5]] },
          "$class_days",
        ],
      },
    },
  },
  {
    $set: {
      class_days: {
        $cond: [
          { $eq: ["$FRIDAY_CDE", "F"] },
          { $concatArrays: ["$class_days", [6]] },
          "$class_days",
        ],
      },
    },
  },
  {
    $set: {
      class_days: {
        $cond: [
          { $eq: ["$SATURDAY_CDE", "S"] },
          { $concatArrays: ["$class_days", [7]] },
          "$class_days",
        ],
      },
    },
  },
  {
    $set: {
      STUDENT_EMAIL: { $toLower: "$STUDENT_EMAIL" },
    },
  },
  {
    $set: {
      PROFESSOR_EMAIL: { $toLower: "$PROFESSOR_EMAIL" },
    },
  },
  {
    $addFields: {
      c_code: { $replaceAll: { input: "$CRS_CDE", find: " ", replacement: "" } }
    }
  },
  { $out: "student-schedules" },
]