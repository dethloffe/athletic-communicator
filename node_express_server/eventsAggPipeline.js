// This is run against the (sporting) events to add some fields that are more uniform across documents.
// This is important for comparisons' sake.
module.exports = [
  {
    $set: {
      Date: {
        $cond: [
          { $regexMatch: { input: "$Date", regex: /(.*)(\d+)(.*)/ } },
          "$Date",
          null
        ]
      },
    },
  },
  {
    $set: {
      Time: {
        $cond: [
          { $regexMatch: { input: "$Time", regex: /(.*)(\d+)(.*)/ } },
          "$Time",
          null
        ]
      },
    },
  },
  {
    $set: {
      "Departure Time": {
        $cond: [
          { $regexMatch: { input: "$Departure Time", regex: /(.*)(\d+)(.*)/ } },
          "$Departure Time",
          null
        ]
      },
    },
  },
  {
    $addFields: {
      full_datetime: { $concat: ["$Date", " ", "$Departure Time"] },
    },
  },
  {
    $addFields: {
      formatted_full_datetime: {
        $dateFromString: { dateString: "$full_datetime", onError: null },
      },
    },
  },
  {
    $addFields: {
      formatted_date: { $dateFromString: { dateString: "$Date", onError: null } },

    },
  },
  {
    $addFields: {
      day: { $dayOfWeek: "$formatted_date" }
    }
  },
  {
    $addFields: {
      dayOfMonth: {$dayOfMonth: "$formatted_date" },
      month: {$month: "$formatted_date" },
      year: {$year: "$formatted_date" },
    }
  },
  {
    $out: "events"
  },
]