module.exports = [
    {
        $set: {
            "Student Email": { $toLower: "$Student Email" },
        },
    },
    { $out: "athletes" },
]