export dir=/home/ad.msoe.edu/yoder/athcomm/
export log=$dir/log.txt
cd $dir
echo "" >> $log
echo "Automatically starting Mongod and the Athcomm docker image on system startup" >> $log
date >> $log
echo "Starting mongod" >> $log 2>&1
sudo systemctl start mongod >> $log 2>&1
echo "Launching docker image" >> $log 2>&1
sudo docker run --rm --log-driver=journald --network=host -d -e WEB_PASSWORD=$password -e TESTING_EMAIL=0 -e TEST_EMAIL="yoder@msoe.edu" --name athcomm athcomm >> $log 2>&1
