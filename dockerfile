FROM node:16.18-bullseye

# Install required libraries
RUN apt update; apt install -y wget msmtp
    # nginx

RUN wget https://fastdl.mongodb.org/tools/db/mongodb-database-tools-ubuntu2004-x86_64-100.5.2.deb; apt install ./mongodb-database-tools-*-100.5.2.deb

COPY msmtprc /etc/msmtp/


# Expose ports and set Env variables
EXPOSE 80

EXPOSE 4000

ENV NODE_EXPRESS_SERVER_HOST=0.0.0.0
ENV NODE_EXPRESS_SERVER_PORT=4000

EXPOSE 3000

ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000

# ENV DOCKER=1

# copy source files from git repo //TODO: refine these copies

COPY nuxt_website/. /nuxt_website
COPY node_express_server/. /node_express_server
COPY blade_server_functions/. /blade_server_functions

RUN cd /nuxt_website; npm install; npm run build

RUN cd /node_express_server; npm install

RUN cd /blade_server_functions; npm install

RUN mkdir /csv; mkdir /csv/used

COPY entrypoint.sh entrypoint.sh

ENTRYPOINT ["bash", "entrypoint.sh"]
# ENTRYPOINT ["/entrypoint.sh"]
# CMD ["/node_express_server", "node", "index.js"]
# CMD ["/nuxt_website", "npm", "start", "|", "bash", ";", "/node_express_server", "node", "index.js", "|", "bash"]