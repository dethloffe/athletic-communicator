# Top level project
[package.json](./package.json)
|name|version|notes|
|---|---|---|
|axios|^0.26.0||

# Node-Express Server
[package.json](./node_express_server/package.json)
|name|version|notes|
|---|---|---|
|axios|^0.26.0||
|axios|^0.24.0||
|commander|^2.20.3||
|csv|^1.1.0||
|csv-parser|^3.0.0||
|express|^4.17.2||
|mongo|^0.1.0||
|mongodb|^4.1.4||
|node-cron|^3.0.0||

# Nuxt Website
[package.json](./nuxt_website/package.json)
|name|version|notes|
|---|---|---|
|@nuxtjs/axios|^5.13.6||
|core-js|^3.19.3||
|express|^4.17.1||
|multer|^1.4.4||
|nuxt|^2.15.8||
|vue|^2.6.14||
|vue-server-renderer|^2.6.14||
|webpack|^4.46.0||

# Blade Server Functions (not really used anymore)
[package.json](./nuxt_website/package.json)
|name|version|notes|
|---|---|---|
|axios|^5.13.6||
|mongodb|^4.3.0||
|node-cron|^3.0.0||

# Docker Production image
[dockerfile](./dockerfile)
|name|version|notes|
|---|---|---|
|Node Image|node:lts-bullseye|[docker image](https://hub.docker.com/layers/node/library/node/lts-bullseye/images/sha256-93b3984b6d440ff5cc3cc176133a30d3944fb0545158655ebaa2180c1096fb5f?context=explore)|
|wget|*||
|msmtp|*||
|mongodb-database-tools|*||

# Docker Dev image
[dockerfile.dev](./dockerfile.dev)
|name|version|notes|
|---|---|---|
|Node Image|node:lts-bullseye|[docker image](https://hub.docker.com/layers/node/library/node/lts-bullseye/images/sha256-93b3984b6d440ff5cc3cc176133a30d3944fb0545158655ebaa2180c1096fb5f?context=explore)|
|wget|*||
|msmtp|*||
|mongodb-database-tools|*||