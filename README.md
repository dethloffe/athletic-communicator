# Table of contents

- [Introduction](#introduction)
- [Building/Running Docker Image Locally](#buildingrunning-docker-image-locally)
  - [Build docker image locally](#build-docker-image-locally)
  - [Run docker image locally](#run-docker-image-locally)
- [Running images from Gitlab Container Registry](#running-images-from-gitlab-container-registry)
  - [Login to the gitlab container registry](#login-to-the-gitlab-container-registry)
  - [Running dev branch (local)](#running-dev-branch-local)
  - [Running main branch (local)](#running-main-branch-local)
  - [Deploying dev or main branch to MSOE Blade Server](#deploying-dev-or-main-branch-to-msoe-blade-server)
  - [Manually running docker image on Blade Server from gitlab contianer registry](#manually-running-docker-image-on-blade-server-from-gitlab-contianer-registry)
- [Setting up MSOE Blade Server production Environment](#setting-up-msoe-blade-server-production-environment)
  - [1. install/setup docker](#1-installsetup-docker)
  - [2. install/setup mongodb](#2-installsetup-mongodb)
  - [3. install/setup Nginx](#3-installsetup-nginx)
  - [4. install Gitlab Runner on MSOE Blade Server](#4-install-gitlab-runner-on-msoe-blade-server)
  - [Commands to start/stop/restart gitlab runner service](#commands-to-startstoprestart-gitlab-runner-service)
  - [Gitlab Runner Reference:](#gitlab-runner-reference)
- [Setting up local development environment](#setting-up-local-development-environment)

# Introduction
Welcome to the Athletic Communicator project.  This intro was written by the previous year's team with some edits by Dr. Yoder to reflect the Product Owner (PO) Dr. Anne-Marie Nickel's plans for the project for this year. 

Currently, the Athletic Communicator, or AthComm, coordinates class and sports schedules of student-athletes to determine which classes student-athletes will be missing due to a sporting competition, and notifies professors accordingly. These "conflicts", as their referred to throughout the project, are determined by cross-checking student-athletes competition schedules with sports rosters and the student-athletes class schedule. Conflicts, along with the information needed to determine them, are all contained within a MongoDB database. The database is populated by the uploading of CSV files (typically will be done by a member of the athletic department e.g., the athletic secretary - assume low technical ability) through the AthComm website.

Email notifications to professors are a key component of AthComm, and are the main functionality. In the 2021-2022 school year, no emails were sent to professors. Email functionality is working and has been tested with the development team members' emails, but no "dry run" or "testing run" with real professor and student data has happened yet. This is the next logical step in the progression of AthComm. **Dr. Yoder plans to request a limited trial run of the software during the Fall 2022 quarter.**  If this is approved, it will be a major step towards receiving official deployment approval from the university, which we hope to achieve by the end of the year.

One of the PO's desired epics for the current year is to allow editing of the uploaded files. Currently, the conflict-determining aspect of the project is not very flexible. For example, if a conflict were to change (say a game was cancelled or the time was moved), there is no way to acknowledge that change in AthComm (save for re-uploading a schedule).  Such edits would be useful to quickly respond to game cancellations, delays, or changes to the venue.

This will require re-considering the project's architecture and proposing revisions to support editing.  There are a variety of directions this may take.

One possibility is that the uploaded schedules could be edited without having to clear out the database and re-upload all the files.

Another possibility would be adding functionality to add conflicts in a one-off instance. This could be done relatively straight-forwardly by adding functionality on the front end to add a new event document to the database. In addition, being able to remove conflicts is functionality worth adding.

While the current system considers departure time when determining what counts as a conflict, it may be good to revisit this feature and make adjustments to it.

The majority of your direction will come from Dr. Anne-Marie Nickel, who is a professor in the Physics and Chemistry department and is the MSOE Athletic Department Faculty Representative. This basically means she communicates with the athletic department on behalf of the school's faculty, and this project originated with and continues to go through her.

With that, we will leave you with some suggestions for possible functionality that can be added to the project. Some might be just a PBI, others might be a whole sprint, and there will be varying degrees of complication. Some ideas are:

 - Allowing multiple file uploads in one instance of File Explorer e.g., being able to upload every roster in one instance of File Explorer. 
 - Adding functionality to remove an instance of a conflict in the case of cancellation.
 - Adding functionality to add an instance of an event in order to determine any new conflicts. 
 - Removing extraneous data from the database.
	 - There will be extra fields in the files uploaded by the athletic secretary. Removing these fields would be valuable from a space point of view.
 - Refactoring `Button.vue` into a proper page[s]. `Button.vue` being as large as it is technical debt from learning the Vue framework and should be refactored to match the rest of the project.
 - Adding user types. For example, having professors being able to login to see the conflicts, but not being able to upload or delete files.
 - Displaying all of the athletes for a given professor.
 - More robust password system (forgot password, 2FA, etc.)
 - If you add professor user types, talking to Dr. Nickel about a link to the conflicts page for a professor to filter with.


# System Overview
## Nuxt web application
The frontend of this project is a Nuxt web app, this is a javascript framework meant to simplify the development of building web applications with Vue, and handles much of the boilerplate code you would need to write to get a simple prject up and running.

The main portion of the project where the Javascript and HTML resides is in the "pages" folder under the "nuxt_website" folder. Each .vue file in this folder represents a single page of the website.

This frontend nuxt web application is deployed as a nodejs process in a docker container. This means there is a running process on a server sending data when clients connect to the website and perform actions that require updating the view.

The business logic of the athcomm system is handled in the Node Express Server. The nuxt webs app communicates with this Node Express Server by utilizing the nuxt api feature. The api folder contains .js files which each represent an api endpoint. These api endpoints are what the web application frontend calls to perform an action or request some data. These endpoints then send requests to the actual Node Express Server. Also in this folder is an index.js file which is where all of the api endpoints are initialized and added to the nuxt application so they can be used.


## Node Express Server
The Node Express Server is where all the business logic of the athcomm system resides. This includes getting emails, conflicts, and uploaded file info from the MongoDB database, uploading/processing CSV files, generating data based on uploaded files, resetting the database, and sending emails on a schedule.

The Nuxt web application api endpoint files found in the /api folder interact with the endpoints found in the index.js file in the node_express_server folder.

Developing the nuxt website while being connected to the node express server is required to test the full functionality of the system. See the sections below for starting and using docker down below for more details.

## Before starting development

before starting development it is recommended to prepare your local development environment and msoe blade serve (or other linux server where the project will be deployed).

Follow the instructions under [Setting up MSOE Blade Server production Environment](#Setting up MSOE Blade Server production Environment) to set up the production environment, and the instructuions under [Setting up local development environment](#setting-up-local-development-environment) for setting on an SSH connection to the blade server, instlaling docker/WSL, instlaling MongoDB/MongoDB Compass, and starting up the project locally for the first time.

## Notes on running the docker images:


you can add the environment variables WEB_PASSWORD and TEST_EMAIL along with any run command to change the behavior of these functions.

set WEB_PASSWORD to any string you would like to set for the website login password with the "admin" user:

```
-e WEB_PASSWORD="somepasswordstring"
```

set TESTING_EMAIL to 0 to enable sending of emails to professors, or set to 1 to disable sending emails to professors. (set to 1 in code by default)

```
-e TESTING_EMAIL=0
```

set TEST_EMAIL to change where test emails are sent, default is athcomm teams channel if unset
```
-e TEST_EMAIL="someone@msoe.edu"
```

For example:
(running docker image locally with manual setting of password and sending test emails)
```
docker run -d -p 3000:3000/tcp -p 4000:4000/tcp -e LOCAL=1 -e TESTING_EMAIL=1 -e TEST_EMAIL="name@msoe.edu" -e WEB_PASSWORD="password" --name athcomm athcomm
```

# Building/Running Docker Image Locally

#### Only need to run with --no-cache if you have issues with files not updating when building docker image

\*use sudo on linux if there are permission errors

\*\*try building with and without no-cache, use no-cache if system is not updating

## building athomm project
```
docker build -t athcomm .
```

## run docker image locally

```
docker rm -f athcomm; docker build --no-cache --build-arg GITLAB_TOKEN=<TOKEN GOES HERE> --build-arg BRANCH=$(git branch --show-current) -f dockerfile.dev . -t athcomm; docker run -d -p 3000:3000/tcp -p 4000:4000/tcp -e LOCAL=1 --name athcomm athcomm
```

## running in dev mode locally:

if you build the "dev version" of the athcomm project, you now have a dev encironment image set up and ready to use for development of the poject.

If you do not already, install the "docker" vscode extension. If you have the dev version of the projhect running in docker locally you will see a containers tab.

Once installed in vscode click the "Docker" tab on the left, you will see the containers tab on top.

If you have not already, start the docker image.

You should now see a listing with a green arrow to the left of "athcomm", right-click this entry and clock "Attach Visual Studio Code". A second vscode window will open with your developer environment ready to use.

Before exiting this container image always remember to commit and push all changes.


## building dev version (see running in dev mode locally for more details)
```
docker build --build-arg GITLAB_TOKEN=<Gitlab personal access token> -f dockerfile.dev . -t athcomm
```

## running dev image locally (same as normally running docker image locally)

```
docker run -d -p 3000:3000/tcp -p 4000:4000/tcp -e LOCAL=1 --name athcomm athcomm
```

# Running images from Gitlab Container Registry

\*use sudo on linux if there are permission errors

## Login to the gitlab container registry

```
docker login registry.gitlab.com
```

### run the docker image from gitlab container registry with desired branch tag (locally)

## Running dev branch (local)

```
docker run --pull=always -d -p 3000:3000/tcp -p 4000:4000/tcp -e LOCAL=1 --name athcomm registry.gitlab.com/msoe.edu/sdl/y22sdl/sdl-athcomm/athcomm:dev
```

## Running main branch (local)

```
docker run --pull=always -d -p 3000:3000/tcp -p 4000:4000/tcp -e LOCAL=1 --name athcomm registry.gitlab.com/msoe.edu/sdl/y22sdl/sdl-athcomm/athcomm:main
```

## Deploying dev or main branch to MSOE Blade Server

After committing changes and pushing to the remote git repository a gitlab CI/CD pipeline will start. Once the "build" stage is done the docker image in the gitlab container registry will be updated.

To actually deploy this new image to the MSOE Blade server you must navigate to the CI/CD pipelines page, find the desired pipeline to deploy, and select the deploy button on the second stage of the pipeline. (see image below)

![alt text](./pipeline_01.png "Title")

## Manually running docker image on Blade Server from gitlab contianer registry

#### Running dev branch

```
docker run --pull=always --rm -d --network="host" --name athcomm registry.gitlab.com/msoe.edu/sdl/y22sdl/sdl-athcomm/athcomm:dev
```

#### Running main branch

```
docker run --pull=always --rm -d --network="host" --name athcomm registry.gitlab.com/msoe.edu/sdl/y22sdl/sdl-athcomm/athcomm:main
```

# Setting up MSOE Blade Server production Environment

## 1. install/setup docker

##### (container runtime)

//TODO
follow steps 1-3 for "Set up the repository" and then step 1 for "Install Dokcer Engine" https://docs.docker.com/engine/install/ubuntu/

## 2. install/setup mongodb

##### (database)

follow steps 1-4 found here for installing mongodb on ubuntu 20.04 (Focal)

https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-ubuntu/

## 3. install/setup Nginx

##### (reverse proxy)

### 1. Install Nginx
```bash
sudo apt-get install nginx
```
### 2. Remove default config file
```bash
sudo rm /etc/nginx/sites-enabled/default
```
### 3. Create new config file
```bash
sudo nano /etc/nginx/sites-available/reverse-proxy
```
### 4. paste the following configuration into the new file
```
upstream athcomm {
        server 127.0.0.1:3000;
}

server {
  listen 80;
  server_name athcomm.sdlstudentvm03.msoe.edu;

  location / {
        proxy_pass http://athcomm;
    }
}
```

### 5. symlink file to sites-enabled folder
```bash
sudo ln -s /etc/nginx/sites-available/reverse-proxy /etc/nginx/sites-enabled/reverse-proxy
```

### 6. restart nginx service with new config
```bash
sudo service nginx restart
```

```
docker run --pull=always -d -p 3000:3000/tcp -p 4000:4000/tcp -e LOCAL=1 --name athcomm registry.gitlab.com/msoe.edu/sdl/y22sdl/sdl-athcomm/athcomm:main
```

## 4. install Gitlab Runner on MSOE Blade Server

##### (automated deployment solution)

### 1. download gitlab runner .deb package

```bash
$ curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
```

### 2. install gitlab runner package

```bash
$ sudo apt-get install gitlab-runner
```

### 3. Verify installed version

```bash
$ sudo gitlab-runner -version
```

### 4. Check running status of gitlab runner service make sure it's running

```bash
$ sudo gitlab-runner status
```

### 5. Edit the visudo file

```bash
$ sudo visudo
```

### Then add the following lines in the sudoers group as shown below:

### 5.1 Add this line below "`# User privilege specification`"

```
gitlab-runner ALL=(ALL:ALL) ALL
```

### 5.2 And add this line below "`# includedir /etc/sudoers.d`"

```
gitlab-runner ALL=(ALL) NOPASSWD: ALL
```

### 6. Register gitlab runner

```bash
$ gitlab-runner register
```

enter: `https://gitlab.com/`

enter: `<registration token>`

enter: `Gitlab runner for athcomm project`

enter: `msoe_blade_server`

skip note about maintainance

wait for runner to register

enter: `shell`

run: `sudo gitlab-runner start` to make sure the gitlab runner is started

---

The gitlab runner is now set up on the linux server and will run the "deploy" job on each gitlab CI/CD pipeline execution.

\*Note, the deploy stage/job is a manual step! only deploy if desired. Check gitlab pipelines page to manually run stage

//TODO: possibly change this behavior in the future to perhaps automated on certain branches only?

---

## Commands to start/stop/restart gitlab runner service

```
sudo gitlab-runner start
```

```
sudo gitlab-runner stop
```

```
sudo gitlab-runner restart
```

## Gitlab Runner Reference:

https://docs.gitlab.com/runner/install/

https://docs.gitlab.com/runner/register/#linux

https://docs.gitlab.com/runner/executors/

# Setting up the sdlstudentvm02.msoe.edu test-deployment VM

## Ensure filewall properly configured (ESSENTIAL FOR SECURITY)
WE MUST ENSURE THE FIREWALL IS PROPERLY CONFIGURED!!!!!

Run this command to confirm the firewall is enabled:
```
sudo ufw status # to see if enabled
```

This should produce the following output
```
Status: active

To                         Action      From
--                         ------      ----
#3000                       ALLOW       Anywhere
22/tcp                     ALLOW       Anywhere    # THIS IS FOR SSH LOGIN. VERY IMPORTANT!
3000 (v6)                  ALLOW       Anywhere (v6)
22/tcp (v6)                ALLOW       Anywhere (v6)
```

If you do NOT see the output above, you must configure ufw to allow the ports we need:
```
sudo ufw enable 
sudo ufw allow 3000 # to allow visibility of port 3000
sudo ufw allow ssh # Equivalent to sudo ufw allow 22/tcp
                   # https://linuxhint.com/ufw-firewall-allow-ssh/
				   # This is critical so you can log in again later.
```
After running these commands, run the `sudo ufw status` command as discussed above to ensure the setup is correct.


## Install mongodb

Use these commands to install mongodb onto the VM:
```
wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
lsb_release -dc # Shows we are on Ubuntu 20.04.4 LTS
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list
less /etc/apt/sources.list.d/mongodb-org-5.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org
echo "mongodb-org hold" | sudo dpkg --set-selections
echo "mongodb-org-database hold" | sudo dpkg --set-selections
echo "mongodb-org-server hold" | sudo dpkg --set-selections
echo "mongodb-org-shell hold" | sudo dpkg --set-selections
echo "mongodb-org-mongos hold" | sudo dpkg --set-selections
echo "mongodb-org-tools hold" | sudo dpkg --set-selections
ps --no-headers -o comm 1 # This revealed we are using systemd
sudo systemctl start mongod
```

After the initial install, the following commands are useful to check on or restart mongod
```
sudo systemctl start mongod
sudo systemctl restart mongod
sudo systemctl status mongod
sudo systemctl stop mongod
```

Run `mongosh` (without arguments) to test that you can connect to the mongodb server locally.

## Set up crontab to start mongod and the docker image on reboot:

Run `sudo /etc/crontab` and add this line to the crontab file:

```
@reboot root export password=REPLACE_WITH_SECURE_PASSWORD; sh /home/ad.msoe.edu/yoder/athcomm/start.sh > /home/ad.msoe.edu/yoder/athcomm/log.tmp.out.txt 2> /home/ad.msoe.edu/yoder/athcomm/log.tmp.err.txt
```

Save the file. Reboot. 

The webpage should now be visible.


# Setting up local development environment

## Set up SSH: (remote console)

1. In Windows, press windows button & type "manage optional features" then press enter
2. Press "add feature"
3. Find and select "OpenSSH Client"
4. Wait for install to finish and restart PC
5. press windows button
6. type `cmd` & hit enter
7. type `ssh <msoe_username>@ad.msoe.edu@sdlstudentvm03.msoe.edu` & hit enter
8. Enter msoe password

*Reminder: you have to be on MSOE Wifi or using Global Protect*

**Script to Sign in to VPN and then SSH in:**
1. Create a new file with the file ending `.cmd`
2. In that file, put:
```
@echo off

cd "C:\Program Files\Palo Alto Networks\GlobalProtect"
PanGPA.exe
timeout 20
cd <default, something like: C:/Users/<msoe_username>
ssh <msoe_username>@ad.msoe.edu@sdlstudentvm03.msoe.edu
```

3. Save file to desktop, double click to run

This will open the VPN (if you are off campus, you need to connect). It will then prompt you to put in your password, which is your MSOE password.

## Set up WSL and Docker Desktop

How to install docker desktop on Windows and start project

1. Download and install Docker Desktop for Windows: https://www.docker.com/products/docker-desktop
2. Restart Windows when prompted
3. Open Docker Desktop and accept licenses
4. If WSL2 is not installed, follow instructions to install/enable WSL2
5. Clone athcomm repo into some location
6. Navigate to Readme.md in the athcomm repo
7. run the "build docker image locally" command

[building athomm project](#building-athomm-project)

8. wait for docker image to build
9. observe "athcomm" image added to the list of available image in docker desktop
10. run the "run docker image locally" command

[run docker image locally](#run-docker-image-locally)

11. observe container start up in docker desktop
12. navigate to localhost:3000 to see website

## Install MongoDB and MongoDB Compass

Follow steps 1-3, select "MongoDB Service", and stop after completing step 3d

https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-windows/#install-mongodb-community-edition 


# Troubleshooting Tips

//TODO: add netcat to docker and add commands to verify service accessability

# Testing framework
Current framework [mocha](https://mochajs.org/)

**MongoDB aggregation pipeline testing:** A few tests have been written to test a few of the aggregation pipelines that the project uses which are located in the node_express_server folder. The tests use the mocha testing framework along side the mongo-unit library to set up a mock database for the test data to reside in. In order to run the tests open a new terminal and set the directory to be within the athcomm folder, then enter the command `npm test`. This command will execute a script to run all the mocha tests in the repository. All pipeline tests are currently located in the test folder.
