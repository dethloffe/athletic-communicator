export const state = () => ({
    authenticated: false
})

export const mutations = {
    setAuthenticated(state, loginStatus){
        state.authenticated = loginStatus
    }
}

export const getters = {
    isAuthenticated(state){
        return state.authenticated
    }
}