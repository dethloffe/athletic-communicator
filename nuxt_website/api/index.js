const express = require('express')
const app = express()

const deleteFile = require('./deleteFile')
const retrieveData = require('./retrieveData')
const updateMongoDB = require('./updateMongoDB')
const fileRead = require('./inputtedFile')
const fileParse = require('./requestFileParse')
const conflicts = require('./conflicts')
const emails = require('./emails')
const loginCombo = require('./processLogin')
const deleteDocuments = require('./deleteDocuments')
const files_uploaded = require('./files_uploaded')
const healthping = require('./healthping')
const changeEmailNotifs = require('./changeEmailNotifications')
const verifyLogin = require('./verifyLogin')
app.use(deleteFile)
app.use(retrieveData)
app.use(updateMongoDB)
app.use(fileRead)
app.use(fileParse)
app.use(conflicts)
app.use(emails)
app.use(loginCombo)
app.use(deleteDocuments)
app.use(files_uploaded)
app.use(healthping)
app.use(changeEmailNotifs)
app.use(verifyLogin)


let hostname = "localhost"

if (require.main === module) {
  const port = 3000
  app.listen(port, hostname, () => {
    console.log("[Nuxt_Back] API server listening on port ${port}")
  })
}

module.exports = app