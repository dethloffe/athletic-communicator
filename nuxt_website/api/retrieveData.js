import axios from "axios";
//Creates the express server on the back end
const { Router } = require("express");
const router = Router();

//Actually uploads inputted file to local machine/backend!
router.get("/retrieveData", async (req, res) => {
  let dataWanted = req.query.dataWanted;

  console.log("[Nuxt_back] Getting ", dataWanted);
  console.log(req.url, req.params, req.query);

  let url = "http://localhost:4000"

  // token verification code
  let verified = false;
  console.log(`login token ${req.query.token}`)
  if (req.query.token !== '') {
    verified = await axios
    .get(url + "/verifyHash", {
      params: {
        token: req.query.token
      },
    })
    .then((data) => {
      console.log(data.data)
      return data.data
    }).catch(function (error) {
      console.log(error)
    });
  }
  console.log(`Token verified: ${verified}`)
  if (!verified) {
    res.send(false);
    return;
  }

  await axios
    .get(url + "/retrieveData", {
      params: req.query,
    })
    .then((data) => {
      res.send(data.data)
    }).catch(function (error) {
      console.log(error)
      res.send(error)
    });
});

module.exports = router;