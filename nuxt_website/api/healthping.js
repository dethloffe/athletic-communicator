import axios from "axios";
//Creates the express server on the back end
const { Router } = require("express");
const router = Router();

//Actually uploads inputted file to local machine/backend!
router.get("/healthping", async (req, res) => {
  console.log(req.url, req.params, req.query);
  
  let url = "http://localhost:4000"

  await axios
    .get(url + "/")
    .then((data) => {
      console.log("[Nuxt_Back] health ping succeeded!");
      console.log(data.data)
      res.json(data.data)
    }).catch(function (error) {
      console.log("[Nuxt_Back] health ping failed :(");
      console.log(error)
      res.json({nodeExpressStatus: false, mongodbConnectionStatus: false})
    });
});

module.exports = router;
