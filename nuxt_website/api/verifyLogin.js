//Creates the express server on the back end
const { Router } = require("express");
const router = Router();
//Needed to parse multi-part files as uploaded in Axios
const axios = require("axios");
const path = require('path');

//Actually uploads inputted file to local machine/backend!
router.get("/verifyLogin", async (req, res) => {
  console.log(req.url, req.params, req.query);
  res.send(await verifytoken(req.query));
});

let url = "http://localhost:4000"

if (process.env.WEB_PASSWORD) {
  password = process.env.WEB_PASSWORD
} else {
  password = "roscoe2022"
}

console.log("[Nuxt_Back] password is set (omitted for security)")

async function verifytoken(query) {
  console.log('checkLogin reached')
  if (query.username === "admin" && query.password === password) {
    console.log(password)
    return axios.get(url + "/getHash", {
        params: {
          token: query.token
        }
      })
      .then((data) => {
        console.log("[Nuxt_Back] very veryified that hash");
        console.log(data.data)
        return data.data;
      }).catch(function (error) {
        console.log("[Nuxt_Back] it failed and who knows why");
        console.log(error)
      });
  } else {
    return false
  }
}

module.exports = router;
