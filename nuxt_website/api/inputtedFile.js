//Creates the express server on the back end
const { Router } = require("express");
const router = Router();
//Needed to parse multi-part files as uploaded in Axios
const multer = require("multer");
const axios = require("axios");
const path = require('path');

var storage = multer.diskStorage({
  destination: "./csv/", 
  filename: function (req, file, cb) { cb(null, file.originalname); },
});

var upload = multer({ storage: storage });

//Actually uploads inputted file to local machine/backend!
router.post("/inputtedFile", upload.single("file"), function (req, res) {
  console.log("[Nuxt_Back] GOT HERE FOR SURE")
  console.log("[Nuxt_Back] inputtedFile", req.file);
  res.json({ file: req.file });
});

module.exports = router;
