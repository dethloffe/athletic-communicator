import axios from "axios";
//Creates the express server on the back end
const { Router } = require("express");
const router = Router();

router.get("/changeEmailNotifications", async (req, res) => {
    console.log(req.query);
    let url = "http://localhost:4000"

    // token verification code
    let verified = false;
    console.log(`login token ${req.query.token}`)
    if (req.query.token !== '') {
        verified = await axios
            .get(url + "/verifyHash", {
                params: {
                    token: req.query.token
                },
            })
            .then((data) => {
                console.log(data.data)
                return data.data
            }).catch(function (error) {
                console.log(error)
            });
    }
    console.log(`Token verified: ${verified}`)
    if (!verified) {
        res.send(false);
        return;
    }

    await axios
        .get(url + "/changeEmailNotifications", {
            params: req.query,
        });

    res.send("Complete");
});

module.exports = router;
