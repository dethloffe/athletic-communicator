const axios = require("axios");
const url = "http://localhost:4000"

export default function ({ store, redirect }) {
  // console.log(store.getters["tempAuth/isAuthenticated"])
  if (!store.getters["tempAuth/isAuthenticated"]) {
    return redirect('/login')
  }
}