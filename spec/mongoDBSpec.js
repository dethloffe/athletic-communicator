const { connected } = require('process');

it("should successfully connect to the MongoDB server without crashing.", function () {
    let connected = false;
    try {
        // MongoDB
        const { MongoClient } = require('mongodb');
        const assert = require("assert");
        const { exec } = require("child_process");
        var path = require('path');
        const dbName = "athcomm";
        conflictCollection = "conflicts";
        emailCollection = "emails_to_send";
        let uri;
        uri = "mongodb://localhost:27017";
        const client = new MongoClient(uri, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        const db = client.db(dbName);
        client.connect();
        console.log("Successfully connected to MongoDB server");
        connected = true;
    } catch (err) {
        connected = false;
    } finally {
        expect(connected).toBe(true);
    }
});